package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class ScormCourseViewObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath ="//h5[contains(text(),'Course View')]//following::p[1]")
	public WebElement title;
	@FindBy(xpath ="//h5[contains(text(),'Course View')]//following::p[2]")
	public WebElement grade;
	@FindBy(xpath ="//h5[contains(text(),'Course View')]//following::p[3]")
	public WebElement semester;
	@FindBy(xpath ="//h5[contains(text(),'Course View')]//following::p[4]")
	public WebElement syllabus;
	@FindBy(xpath ="//h5[contains(text(),'Course View')]//following::p[5]")
	public WebElement subject;
	@FindBy(xpath ="//h5[contains(text(),'Course View')]//following::p[6]")
	public WebElement country;
	@FindBy(xpath ="//h5[contains(text(),'Course View')]//following::p[7]")
	public WebElement language;
	@FindBy(xpath ="//h5[contains(text(),'Course View')]//following::p[8]")
	public WebElement description;
	

	public ScormCourseViewObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
}
