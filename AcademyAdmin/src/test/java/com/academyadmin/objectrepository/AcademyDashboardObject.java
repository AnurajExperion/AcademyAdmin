package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class AcademyDashboardObject extends SetUp {
	WebDriver driver;

	@FindBy(id = "nav-icon2")
	public WebElement burgerMenu;
	@FindBy(xpath = "//span[@class='ng-star-inserted']")
	public WebElement experionAdmin;
	@FindBy(xpath = "//a[@class='logo-font']")
	public WebElement copyright;
	@FindBy(xpath = "//span[contains(text(),'SCORM Courses')]")
	public WebElement scormCourse;
	@FindBy(xpath = "//a[contains(text(),'Add SCORM Course')]")
	public WebElement addScormCourse;
	@FindBy(xpath = "//a[contains(text(),'List SCORM Courses')]")
	public WebElement listScormCourses;
	@FindBy(xpath = "//li[5]//div[2]//ul[1]//li[3]//a[1]")
	public WebElement bulkUpload;
	@FindBy(xpath = "//span[contains(text(),'Learning object management')]")
	public WebElement learningObjectManagement;
	@FindBy(xpath = "//a[contains(text(),'Add Learning Object')]")
	public WebElement addLearningObjects;
	@FindBy(xpath = "//a[contains(text(),'List Learning Objects')]")
	public WebElement listLearningObjects;
	@FindBy(xpath = "//span[contains(text(),'Question management')]")
	public WebElement questionManagement;
	@FindBy(xpath = "//a[contains(text(),'Add Question')]")
	public WebElement addQuestion;
	@FindBy(xpath = "//a[contains(text(),'List Questions')]")
	public WebElement listQuestion;
	@FindBy(xpath = "//li[3]//div[2]//ul[1]//li[3]//a[1]")
	public WebElement questionBulkUpload;
	@FindBy(xpath = "//span[contains(text(),'Course management')]")
	public WebElement courseManagement ;
	@FindBy(xpath = "//a[contains(text(),'Add Course')]")
	public WebElement addCourse ;
	@FindBy(xpath = "//a[contains(text(),'List Courses')]")
	public WebElement listCourse;
	@FindBy(xpath = "//span[contains(text(),'Past Exam management')]")
	public WebElement pastExamManagement;
	@FindBy(xpath = "//a[contains(text(),'Add Past Exam')]")
	public WebElement addPastExam;
	@FindBy(xpath = "//a[contains(text(),'List Past Exam')]")
	public WebElement listPastExam;
	@FindBy(xpath = "//a[contains(text(),'Add Comprehension')]")
	public WebElement addComprehension;
	@FindBy(xpath = "//a[contains(text(),'List Comprehension')]")
	public WebElement listComprehension;
	@FindBy(xpath = "//span[contains(text(),'Offer management')]")
	public WebElement offerManagement ;
	@FindBy(xpath = "//a[contains(text(),'Add Offer')]")
	public WebElement addOffer;
	@FindBy(xpath = "//a[contains(text(),'List Offer')]")
	public WebElement listOffer;
	@FindBy(xpath = "//span[contains(text(),'Classroom management')]")
	public WebElement classroomManagement;
	@FindBy(xpath = "//a[contains(text(),'Add Classroom')]")
	public WebElement addClassroom;
	@FindBy(xpath = "//a[contains(text(),'List Classrooms')]")
	public WebElement listClassroom;
	@FindBy(xpath = "//span[contains(text(),'Experts management')]")
	public WebElement expertsManagement;
	@FindBy(xpath = "//a[contains(text(),'Add Expert')]")
	public WebElement addExperts;
	@FindBy(xpath = "//a[contains(text(),'List Experts')]")
	public WebElement listExperts;
	@FindBy(xpath = "//span[contains(text(),'Schedule management')]")
	public WebElement scheduleManagement;
	@FindBy(xpath = "//a[contains(text(),'Add Schedule')]")
	public WebElement addSchedule ;
	@FindBy(xpath = "//a[contains(text(),'List Schedule')]")
	public WebElement listSchedule;
	
	
	
	public AcademyDashboardObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
}
