package com.academyadmin.objectrepository;

import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ProfileObjectAndroid extends SetUp  {
	AppiumDriver<MobileElement> androidDriver;	

	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/right_image_burger")
	public AndroidElement edit;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/prefLanguage")
	public AndroidElement prefLanguage;
	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView")
	public AndroidElement english;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/submit_text")
	public AndroidElement save;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/firstName")
	public AndroidElement fullName;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/email")
	public AndroidElement email;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/prefixText")
	public AndroidElement prefix;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/mobile")
	public AndroidElement mobile;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/eduLevel")
	public AndroidElement grade;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/logout_text")
	public AndroidElement logout;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/menu_image")
	public AndroidElement editProfileBack;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/left_image_burger")
	public AndroidElement profileBack;
	
	public ProfileObjectAndroid(AppiumDriver<MobileElement> androidDriver){
		this.androidDriver = androidDriver;
		PageFactory.initElements(new AppiumFieldDecorator(this.androidDriver), this);	
	}
}
