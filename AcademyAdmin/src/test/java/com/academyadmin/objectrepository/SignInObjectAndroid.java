package com.academyadmin.objectrepository;

import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SignInObjectAndroid extends SetUp{
	AppiumDriver<MobileElement> androidDriver;	

	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText")
	public AndroidElement email;
	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText")
	public AndroidElement password;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/go_button") 
	public AndroidElement signIn;
	@AndroidFindBy(id="android:id/button1") 
	public AndroidElement alreadyLoggedInYes;
	
	public SignInObjectAndroid(AppiumDriver<MobileElement> androidDriver){
		this.androidDriver = androidDriver;
		PageFactory.initElements(new AppiumFieldDecorator(this.androidDriver), this);	
	}
	
	public void androidLogin(String email, String password) {
		try {
			this.email.clear();
			this.email.sendKeys(email);
			this.password.clear();
			this.password.sendKeys(password);
			signIn.click();
		}catch(Exception e) {
			e.printStackTrace();
			log.info(e);
		}
	}
}
