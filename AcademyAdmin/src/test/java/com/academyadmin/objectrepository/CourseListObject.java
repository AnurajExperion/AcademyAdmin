package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class CourseListObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath = "//h5[@class='card-title']") public WebElement courseList;
	@FindBy(xpath = "//div[@class='row app-c-add-table__dropdown-block']//div[1]//div[1]//select[1]") public WebElement selectGrade ;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//select[1]") public WebElement selectSemester ;
	@FindBy(xpath = "//div[3]//div[1]//select[1]") public WebElement selectSubject ;
	@FindBy(xpath = "//div[4]//div[1]//select[1]") public WebElement selectStatus;
	@FindBy(xpath = "//div[5]//div[1]//select[1]") public WebElement selectType;
	@FindBy(xpath = "//input[@placeholder='Search by Course Name']") public WebElement searchCourseName ;
	@FindBy(xpath = "//button[@class='btn btn-primary app-u-mr']") public WebElement search;
	@FindBy(xpath = "//button[@class='btn btn-secondary']") public WebElement clear;
	
	@FindBy(xpath = "//tr[1]//td[9]//span[1]//tr[1]//td[1]//i[1]") public WebElement view;
	@FindBy(xpath = "//tr[1]//td[9]//span[1]//tr[1]//td[2]//i[1]") public WebElement edit;
	@FindBy(xpath = "//tr[1]//td[9]//span[1]//tr[1]//td[3]//i[1]") public WebElement delete ;
	
	@FindBy(xpath = "//div[@class='ui-toast-summary']")	public WebElement toastMsg;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Course created successfully')]")	public WebElement courseCreateMsg;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Course deleted successfully')]")	public WebElement courseDeleteMsg;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Course updated successfully')]")	public WebElement courseEditMsg;
	@FindBy(xpath = "//div[@class='ui-toast-detail'][contains(text(),'Please select Subject and Languge to proceed.')]")	public WebElement errorMsg;
	@FindBy(xpath = "//span[contains(text(),'No records found')]")	public WebElement noRecordsFound;
	@FindBy(xpath = "//span[contains(text(),'Yes')]")	public WebElement deleteYes;
	
	
	public CourseListObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
}
