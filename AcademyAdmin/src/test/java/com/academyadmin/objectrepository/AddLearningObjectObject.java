package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class AddLearningObjectObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath = "//label[contains(text(),'Language')]//following::select[1]")
	public WebElement language;
	@FindBy(xpath = "//input[@placeholder='Enter LO Number']")
	public WebElement loNumber;
	@FindBy(xpath = "//input[@placeholder='Enter LO Name']")
	public WebElement loName;
	@FindBy(xpath = "//label[contains(text(),'Subject')]//following::select[1]")
	public WebElement subject;
	@FindBy(xpath = "//label[contains(text(),'Topic')]//following::select[1]")
	public WebElement topic;
	@FindBy(xpath = "//label[contains(text(),'Status')]//following::select[1]")
	public WebElement status;
	@FindBy(xpath = "//input[@class='undefined']")
	public WebElement tags;
	@FindBy(id = "type")
	public WebElement isExpertContent;
	@FindBy(xpath = "//div[@class='ql-editor ql-blank']")
	public WebElement description;
	@FindBy(xpath = "//input[@placeholder='Enter Video Link']")
	public WebElement videoLink;
	@FindBy(xpath = "//button[@class='file btn btn-primary']")
	public WebElement select;
	@FindBy(xpath = "//tr[1]//td[1]//p-tableradiobutton[1]//div[1]//div[2]//span[1]")
	public WebElement firstVideoLink;
	@FindBy(xpath = "//th[contains(text(),'Video Link')]//following::button[contains(text(),'Select')]")
	public WebElement selectVideoBtn;
	@FindBy(xpath = "//span[contains(text(),'Select Video Link')]/following::a[2]")
	public WebElement expandVideo;
	@FindBy(xpath = "//button[contains(text(),'Select Question')]")
	public WebElement selectQuestionBtn;
	@FindBy(xpath = "//span[contains(text(),'Select Questions')]/following::a[2]")
	public WebElement expandQuestion;
	@FindBy(xpath = "//input[@placeholder='Type here']")
	public WebElement typeHere;
	@FindBy(xpath = "//tr[1]//td[1]//p-tablecheckbox[1]//div[1]//div[2]")
	public WebElement selectQuestionChx;
	@FindBy(xpath = "//span[contains(text(),'Select Questions')]//following::button[contains(text(),'Select')]")
	public WebElement qstnSelected;
	@FindBy(xpath = "//button[contains(text(),'Save')]")
	public WebElement save;
	@FindBy(xpath = "//div[contains(text(),'Learning object added successfully.')]")
	public WebElement learningObjectCreateMsg;

	public AddLearningObjectObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void addLearningObject(String language, String loNumber, String loName, String subject, String topic, String status, String tags, 
			Boolean isExpertContent, String description, String typeHere) throws InterruptedException {
		this.language.sendKeys(language);
		this.loNumber.clear();
		this.loNumber.sendKeys(loNumber);
		this.loName.clear();
		this.loName.sendKeys(loName);
		this.subject.sendKeys(subject);
		Thread.sleep(2000);
		waitIfElementClickIsIntercepted(this.topic, "select", topic);
		this.status.sendKeys(status);
		this.tags.clear();
		this.tags.sendKeys(tags);
		if(isExpertContent)
			this.isExpertContent.click();
		this.description.clear();
		this.description.sendKeys(description);
		this.select.click();
		waitIfElementClickIsIntercepted(this.firstVideoLink, "click", "");
		this.expandVideo.click();
		waitForElementToLoad(this.selectVideoBtn);
		this.selectVideoBtn.click();
		if(!isExpertContent) {
			waitForElementToLoad(this.selectQuestionBtn);
			this.selectQuestionBtn.click();
			this.typeHere.click();
			this.typeHere.sendKeys(typeHere);
			Thread.sleep(2000);
			this.selectQuestionChx.click();
			this.expandQuestion.click();
			waitForElementToLoad(this.qstnSelected);
			this.qstnSelected.click();
		}		
		this.save.click();
	}
	
}
