package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class ScormCourseListObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath = "//h5[@class='card-title']") public WebElement scormCourseList;
	@FindBy(xpath = "//div[@class='row app-c-add-table__dropdown-block']//div[1]//div[1]//select[1]") public WebElement selectGrade;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//select[1]")	public WebElement selectSemester;
	@FindBy(xpath = "//div[3]//div[1]//select[1]")	public WebElement selectSubject;
	@FindBy(xpath = "//div[4]//div[1]//select[1]")	public WebElement selectStatus;
	@FindBy(xpath = "//input[@placeholder='Search by Course Name']")	public WebElement searchCourseName;
	@FindBy(xpath = "//button[@class='btn btn-primary app-u-mr']")	public WebElement search;
	@FindBy(xpath = "//button[@class='btn btn-secondary']")	public WebElement clear;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Course created successfully')]")	public WebElement courseCreateMsg;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Course updated successfully')]")	public WebElement courseEditMsg;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'SCORM course was deleted successfully.')]")	public WebElement courseDeleteMsg;
	@FindBy(xpath = "//th[7]//span[contains(text(),'Status')]/following::span[13]")	public WebElement status;
	@FindBy(xpath = "//span[contains(text(),'SCORM Status')]/following::span[12]")	public WebElement scormStatus;
	@FindBy(xpath = "//span[contains(text(),'Actions')]/following::span[10]/tr/td[1]")	public WebElement view;
	@FindBy(xpath = "//span[contains(text(),'Actions')]/following::span[10]/tr/td[2]")	public WebElement edit;
	@FindBy(xpath = "//span[contains(text(),'Actions')]/following::span[10]/tr/td[3]")	public WebElement delete;
	@FindBy(xpath = "//span[contains(text(),'Yes')]")	public WebElement deleteYes;
	@FindBy(xpath = "//span[contains(text(),'No records found')]")	public WebElement noRecordsFound;
	
	public ScormCourseListObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void scormSearch(String selectGrade, String selectSemester, String selectSubject, String searchCourseName) {
		log.info("Entered scormSearch method");
		this.selectGrade.sendKeys(selectGrade);
		this.selectSemester.sendKeys(selectSemester);
		this.selectSubject.sendKeys(selectSubject);
		this.searchCourseName.clear();
		this.searchCourseName.sendKeys(searchCourseName);
		waitIfElementClickIsIntercepted(this.search, "click", "");
	}
	public void scormSearchEdit(String searchCourseName) {
		this.searchCourseName.clear();
		this.searchCourseName.sendKeys(searchCourseName);
		this.search.click();
	}
	
}
