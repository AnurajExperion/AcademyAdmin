package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class AddScheduleObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath = "//input[@placeholder='Enter the title in English']")
	public WebElement titleEnglish;
	@FindBy(xpath = "//input[@placeholder='Enter the title in Arabic']")
	public WebElement titleArabic;
	@FindBy(xpath = "//input[@id='fileInputField1']")
	public WebElement uploadImageEnglish;
	@FindBy(xpath = "//input[@id='fileInputField2']")
	public WebElement uploadImageArabic ;
	@FindBy(xpath = "//select[@class='form-control ng-untouched ng-pristine ng-valid']")
	public WebElement country;
	@FindBy(xpath = "//select[@class='form-control ng-untouched ng-pristine ng-invalid']")
	public WebElement grade;
	@FindBy(xpath = "//input[@placeholder='Enter Live Stream URL']")
	public WebElement liveUrl;
	@FindBy(xpath = "//select[@class='form-control ng-pristine ng-valid ng-touched']")
	public WebElement status;
	@FindBy(xpath = "//button[@class='btn btn-primary']")
	public WebElement save;
	@FindBy(xpath = "//button[@class='btn btn-secondary app-u-mr']")
	public WebElement cancel;
	
	@FindBy(xpath = "//input[@placeholder='Enter the title in English']//following::span[1]")
	public WebElement titleEngMandatoryMsg;
	@FindBy(xpath = "//input[@placeholder='Enter the title in English']//following::span[2]")
	public WebElement titleArabicMandatoryMsg;
	@FindBy(xpath = "//div[@class='form-group']//span[@class='help-block error ng-star-inserted'][contains(text(),'Image is required.')]")
	public WebElement uploadImgEngMandatoryMsg;
	@FindBy(xpath = "//div[3]//div[1]//div[1]//div[2]//div[1]//span[1]")
	public WebElement uploadImgArabMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'URL is required.')]")
	public WebElement urlMandatoryMsg;
	
	
	
	
	public AddScheduleObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
}

