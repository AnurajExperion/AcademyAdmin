package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class SignInObject extends SetUp {	
	WebDriver driver;
	
	@FindBy(id="email")	public	WebElement email;
	@FindBy(id="password") public WebElement password;
	@FindBy(id="login-form-submit")	public	WebElement submit;
	@FindBy(xpath="//span[contains(text(),'Invalid credentials')]") public WebElement invalidCredentials;
	@FindBy(xpath="//img[@class='img-responsive auth-logo']") public WebElement logo;
	
	public SignInObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void signIn(String email, String password) {
		try {
			log.info("Entered signIn method");
			this.email.clear();
			this.email.sendKeys(email);
			this.password.clear();
			this.password.sendKeys(password);
			waitForElementToLoad(submit);
			submit.click();
			//Thread.sleep(2000);
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
}
