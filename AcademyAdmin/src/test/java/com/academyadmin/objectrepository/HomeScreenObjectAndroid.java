package com.academyadmin.objectrepository;

import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomeScreenObjectAndroid extends SetUp {
	AppiumDriver<MobileElement> androidDriver;	

	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/profile_picture")
	public AndroidElement profile;
	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText")
	public AndroidElement password;
	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText")
	public AndroidElement newPassword;
	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText")
	public AndroidElement confirmPassword;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/changePasswordButton")
	public AndroidElement submitPassword;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/let_us_started")
	public AndroidElement welcomePopup1;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/next")
	public AndroidElement welcomePopup2;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/next")
	public AndroidElement welcomePopup3;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/next")
	public AndroidElement welcomePopup4;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/nextButton")
	public AndroidElement familyMemberPopup;
	@AndroidFindBy(xpath="//android.widget.FrameLayout[@content-desc=\"Library\"]/android.widget.ImageView")
	public AndroidElement library;
	
	public HomeScreenObjectAndroid(AppiumDriver<MobileElement> androidDriver){
		this.androidDriver = androidDriver;
		PageFactory.initElements(new AppiumFieldDecorator(this.androidDriver), this);	
	}
}
