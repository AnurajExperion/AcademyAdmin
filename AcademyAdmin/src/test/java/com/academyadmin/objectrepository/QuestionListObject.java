package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class QuestionListObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath = "//a[contains(text(),'Create Question')]") public WebElement createQuestion;
	@FindBy(xpath = "//a[contains(text(),'Bulk upload')]") public WebElement bulkUpload;
	
	@FindBy(xpath = "//div[@class='row app-c-add-table__dropdown-block']//div[1]//div[1]//select[1]") public WebElement selectSubject ;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//select[1]") public WebElement selectTopic;
	@FindBy(xpath = "//div[3]//div[1]//select[1]") public WebElement selectType;
	@FindBy(xpath = "//div[4]//div[1]//select[1]") public WebElement selectAppearedIn;
	@FindBy(xpath = "//h5[contains(text(),'Question List')]//following::select[5]") public WebElement selectStatus;
	@FindBy(xpath = "//input[@placeholder='Search by question']") public WebElement searchByQuestion;
	@FindBy(xpath = "//button[@class='btn btn-primary']") public WebElement search;
	@FindBy(xpath = "//button[@class='btn btn-secondary']") public WebElement clear;

	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Question has been created')]") public WebElement questionCreateMsg ;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Question details have been updated')]") public WebElement questionEditMsg ;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Question has been deleted')]") public WebElement questiondltMsg;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Sorry! You cannot delete a question which is associated with a learning object')]") public WebElement questionDeleteErrorMsg;
	@FindBy(xpath = "//span[contains(text(),'Yes')]")	public WebElement deleteYes;
	@FindBy(xpath = "//span[contains(text(),'No records found')]") public WebElement noRecordsFound;
	@FindBy(xpath = "//tr[1]//td[9]//span[1]//tr[1]//td[1]//i[1]") public WebElement view;
	@FindBy(xpath = "//tr[1]//td[9]//span[1]//tr[1]//td[2]//i[1]") public WebElement edit;
	@FindBy(xpath = "//tr[1]//td[9]//span[1]//tr[1]//td[3]//i[1]") public WebElement delete;
	
	
	public QuestionListObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void questionSearch(String selectSubject, String selectTopic, String selectType, String selectStatus, String question) throws InterruptedException {
		log.info("Entered questionSearch method");
		this.selectSubject.sendKeys(selectSubject);
		Thread.sleep(2000);
		waitIfElementClickIsIntercepted(this.selectTopic, "select", selectTopic);
		this.selectType.sendKeys(selectType);
		this.selectStatus.sendKeys(selectStatus);
		this.searchByQuestion.clear();
		this.searchByQuestion.sendKeys(question);
		waitIfElementClickIsIntercepted(this.search, "click", "");		
	}
}


