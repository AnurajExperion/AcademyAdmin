package com.academyadmin.objectrepository;

import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OnboradingObjectAndroid extends SetUp {
	AppiumDriver<MobileElement> androidDriver;

	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/sign_in_txt2") 
	public AndroidElement login;
	@AndroidFindBy(id="com.betacom.bcmsf.dars:id/eng") 
	public AndroidElement english;
	
	public OnboradingObjectAndroid(AppiumDriver<MobileElement> androidDriver){
		this.androidDriver = androidDriver;
		PageFactory.initElements(new AppiumFieldDecorator(this.androidDriver), this);	
	}
	
}
