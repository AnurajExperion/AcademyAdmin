package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class QuestionViewObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath = "//span[contains(text(),'Question Number')]//following::label[1]")
	public WebElement questionNumber;
	@FindBy(xpath = "//span[contains(text(),'Subject')]//following::label[1]")
	public WebElement subject;
	@FindBy(xpath = "//span[contains(text(),'Topic')]//following ::label[1]")
	public WebElement topic ;
	@FindBy(xpath = "//span[contains(text(),'Language')]//following::label[1]")
	public WebElement language;
	@FindBy(xpath = "//span[contains(text(),'Status')]//following::label[1]")
	public WebElement status;
	@FindBy(xpath = "//span[contains(text(),'Type')]//following::label[1]")
	public WebElement type;
	@FindBy(xpath = "//span[contains(text(),'Display Probability')]//following::label[1]")
	public WebElement displayProbability;
	@FindBy(xpath = "//span[contains(text(),'Appeared In')]//following::label[1]")
	public WebElement appearedIn;
	@FindBy(xpath = "//h5[@class='card-title app-u-mb-sm']//following::p[1]")
	public WebElement question;
	@FindBy(xpath = "//h6[@class='card-title']//following::p[1]")
	public WebElement answerDesciption;
	@FindBy(xpath = "//i[@class='fas fa-check']//preceding::label[1]")
	public WebElement correctAnswer;
	@FindBy(xpath = "//button[@class='btn btn-secondary app-u-mr']")
	public WebElement edit;
	@FindBy(xpath = "//button[@class='btn btn-primary']")
	public WebElement delete;
	
	
	
	public QuestionViewObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
}
