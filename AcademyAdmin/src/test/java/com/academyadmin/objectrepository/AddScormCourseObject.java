package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class AddScormCourseObject extends SetUp {
	WebDriver driver;

	@FindBy(xpath = "//div[@class='card-body']//div[1]//div[1]//div[1]//select[1]")
	public WebElement language;
	@FindBy(xpath = "//input[@placeholder='Enter Title']")
	public WebElement courseTitle;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//div[1]//select[1]")
	public WebElement grade;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[2]//div[1]//select[1]")
	public WebElement semester;
	@FindBy(xpath = "//div[3]//div[1]//div[1]//select[1]")
	public WebElement syllabus;
	@FindBy(xpath = "//div[3]//div[2]//div[1]//select[1]")
	public WebElement subject;
	@FindBy(xpath = "//div[4]//div[1]//div[1]//select[1]")
	public WebElement country;
	@FindBy(xpath = "//label[contains(text(),'Status')]/following::select")
	public WebElement status;
	@FindBy(xpath = "//textarea[@class='form-control ng-untouched ng-pristine ng-valid']")
	public WebElement metaInformation;
	@FindBy(xpath = "//label[contains(text(),'Description')]//following::textarea")
	public WebElement description;
	@FindBy(id = "fileInputField1")
	public WebElement uploadImage;
	@FindBy(xpath = "//div[@class='app-c-edit-question-image ng-star-inserted']")
	public WebElement imagePreview;	
	@FindBy(id = "fileInputField2")
	public WebElement uploadScormCourse;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//div[1]//div[2]//img[1]")
	public WebElement scormPreview;
	@FindBy(xpath = "//button[@class='btn btn-primary']")
	public WebElement addScormCourse;
	@FindBy(xpath = "//i[@class='fa fa-times']")	
	public WebElement deleteScormImage;
	@FindBy(xpath = "//span[contains(text(),'Language is required.')]")
	public WebElement languageMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Course Title is required.')]")
	public WebElement titleMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Grade is required.')]")
	public WebElement gradeMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Semester is required.')]")
	public WebElement semesterMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Syllabus is required.')]")
	public WebElement syllabusMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Subject is required.')]")
	public WebElement subjectMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Country is required.')]")
	public WebElement countryMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Description is required.')]")
	public WebElement descriptionMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Image is required.')]")
	public WebElement imageMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'SCORM file is required.')]")
	public WebElement scormFileMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Course Title must be minimum 3 characters.')]")
	public WebElement titlelengthValidationMsg;
	
	
	public AddScormCourseObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void addScormCourse(String language, String courseTitle, String grade, String semester,String syllabus, String subject, String country, 
			String metaInformation,	String description, String uploadImage, String uploadScormCourse) {
		log.info("Entered addScormCourse method");
		waitForElementToLoad(this.language);
		this.language.sendKeys(language);
		this.courseTitle.clear();
		this.courseTitle.sendKeys(courseTitle);
		this.grade.sendKeys(grade);
		this.semester.sendKeys(semester);
		this.syllabus.sendKeys(syllabus);
		this.subject.sendKeys(subject);
		this.country.sendKeys(country);
		this.metaInformation.clear();
		this.metaInformation.sendKeys(metaInformation);
		this.description.clear();
		this.description.sendKeys(description);
		this.uploadImage.sendKeys(uploadImage);
		waitForElementToLoad(this.imagePreview);
		this.uploadScormCourse.sendKeys(uploadScormCourse);
		fileUploadWait(this.scormPreview);
		waitIfElementClickIsIntercepted(this.addScormCourse, "click", "");
	}
	
	public void editScormCourse(String language, String courseTitle, String grade, String semester,String syllabus, String subject, String country, 
			String metaInformation,	String description) {
		log.info("Entered addScormCourse editScormCourse");
		waitForElementToLoad(this.language);
		this.language.sendKeys(language);
		this.courseTitle.clear();
		this.courseTitle.sendKeys(courseTitle);
		this.grade.sendKeys(grade);
		this.semester.sendKeys(semester);
		this.syllabus.sendKeys(syllabus);
		this.subject.sendKeys(subject);
		this.country.sendKeys(country);
		this.metaInformation.clear();
		this.metaInformation.sendKeys(metaInformation);
		this.description.clear();
		this.description.sendKeys(description);
		waitIfElementClickIsIntercepted(this.addScormCourse, "click", "");
	}
	
	public void titleLengthValidation(String title) {
		log.info("Entered titleLengthValidation method in AddScorm");
		this.courseTitle.clear();
		this.courseTitle.sendKeys(title);		
	}

}
