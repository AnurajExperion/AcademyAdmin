package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class AddCourseObject extends SetUp {

	WebDriver driver;

	@FindBy(xpath = "//div[@class='card-body']//div[1]//div[1]//div[1]//select[1]")
	public WebElement language;
	@FindBy(xpath = "//input[@placeholder='Enter Title']")
	public WebElement courseTitle;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//div[1]//select[1]")
	public WebElement grade;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[2]//div[1]//select[1]")
	public WebElement semester;
	@FindBy(xpath = "//div[3]//div[1]//div[1]//select[1]")
	public WebElement syllabus;
	@FindBy(xpath = "//div[3]//div[2]//div[1]//select[1]")
	public WebElement subject;
	@FindBy(xpath = "//div[4]//div[1]//div[1]//select[1]")
	public WebElement country;
	@FindBy(xpath = "//textarea[@class='form-control ng-untouched ng-pristine ng-valid']")
	public WebElement metaInformation;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//textarea[1]")
	public WebElement description;
	@FindBy(xpath = "//div[6]//div[1]//div[1]//select[1]")
	public WebElement availableFor;
	@FindBy(xpath = "//div[6]//div[2]//div[1]//select[1]")
	public WebElement status;
	@FindBy(xpath = "//input[@id='fileInputField']")
	public WebElement uploadImage;
	@FindBy(xpath = "//div[@class='app-c-edit-question-image ng-star-inserted']")
	public WebElement imagePreview;
	
	@FindBy(xpath = "//button[@class='btn btn-secondary ng-star-inserted']")
	public WebElement addChapter;
	@FindBy(xpath = "//span[contains(text(),'Add Lesson')]")
	public WebElement addLesson;
	@FindBy(xpath = "//span[contains(text(),'Delete Chapter')]")
	public WebElement deleteChapter ;
	@FindBy(xpath = "//input[@id='enable-test-0']")
	public WebElement enableTest ;
	@FindBy(xpath = "//span[contains(text(),'Learning Object')]")
	public WebElement addLearningObject;
	@FindBy(xpath = "//span[@class='ng-tns-c16-3 pi pi-window-maximize']")
	public WebElement maxLearningObject;
	@FindBy(xpath = "//tr[1]//td[1]//p-tablecheckbox[1]//div[1]//div[2]")
	public WebElement individualLearningObject;
	@FindBy(xpath = "//button[contains(text(),'Select')]")
	public WebElement selectLearningObject;
	@FindBy(xpath = "//input[@class='form-control app-c-placeholder-truncate ng-dirty ng-touched ng-invalid']")
	public WebElement exercizeQuestions;
	@FindBy(xpath = "//input[@class='form-control app-c-placeholder-truncate ng-untouched ng-pristine ng-invalid']")
	public WebElement exercizeQuestionsNum;
	
	
	@FindBy(xpath = "//input[@id='enable-quiz-0-0']")
	public WebElement enableQuiz;
	
	@FindBy(xpath = "//div[@class='ng-star-inserted']//div[@class='row']//div[1]//div[1]//input[1]")
	public WebElement quizTitle;
	@FindBy(xpath = "//input[@class='form-control ng-pristine ng-invalid ng-touched']//following::input[1]")
	public WebElement quizNumQuestions;
	@FindBy(xpath = "//input[@class='form-control ng-pristine ng-invalid ng-touched']//following::input[2]")
	public WebElement quizEstimatedTime;
	@FindBy(xpath = "//input[@class='form-control ng-pristine ng-invalid ng-touched']//following::div[8]")
	public WebElement quizDescription;
	
	
	@FindBy(xpath = "//div[@class='ng-star-inserted']//div[@class='row']//div[1]//div[1]//input[1]")
	public WebElement testTitle ;
	@FindBy(xpath = "//div[@class='app-c-course-management__wrapper ng-untouched ng")
	public WebElement testNumQuestions ;
	@FindBy(xpath = "//div[@class='app-c-course-management__chapter-footer ng-untouched ng-pristine ng-invalid']//div[3]//div[1]//input[1]")
	public WebElement  testTime;
	@FindBy(xpath = "//div[@class='ng-star-inserted']//div[@class='ql-editor ql-blank']")
	public WebElement testDescription;
	
	@FindBy(xpath = "//input[@placeholder='Exam Title']")
	public WebElement examTitle;
	@FindBy(xpath = "//input[@placeholder='Exam No of Questions']")
	public WebElement examNumQuestions;
	@FindBy(xpath = "//input[@placeholder='Exam Time']")
	public WebElement examTime ;
	@FindBy(xpath = "//div[@class='ql-editor ql-blank']")
	public WebElement examDescription;
	
	@FindBy(xpath = "//button[@class='btn btn-secondary app-u-mr']")
	public WebElement cancel;
	@FindBy(xpath = "//button[@class='btn btn-primary']")
	public WebElement addCourse;
	
	@FindBy(xpath = "//span[contains(text(),'Language is required.')]")
	public WebElement languageMandatoryMsg ;
	@FindBy(xpath = "//span[contains(text(),'Course Title is required.')]")
	public WebElement courseTitleMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Grade is required.')]")
	public WebElement gradeMandatoryMsg ;
	@FindBy(xpath = "//span[contains(text(),'Semester is required.')]")
	public WebElement semesterMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Syllabus is required.')]")
	public WebElement syllabusMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Subject is required.')]")
	public WebElement subjectMandatoryMsg ;
	@FindBy(xpath = "//span[contains(text(),'Country is required.')]")
	public WebElement  countryMandatoryMsg;
	@FindBy(xpath = "//span[@class='help-block error ng-star-inserted'][contains(text(),'Description is required.')]")
	public WebElement descriptionMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Available For is required.')]")
	public WebElement availableForMandatoryMsg ;
	@FindBy(xpath = "//span[contains(text(),'Status is required.')]")
	public WebElement statusMandatoryMsg ;
	@FindBy(xpath = "//span[contains(text(),'Image is required.')]")
	public WebElement imageMandatoryMsg ;
	
	@FindBy(xpath = "//button[contains(text(),'Update Course')]")
	public WebElement updateCourse ;
	
	
	public AddCourseObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void courseAdd(String language,String courseTitle, String grade, String semester,String syllabus, String subject, String country, 
			String metaInformation,	String description, String availableFor, String status, String uploadImage) {
		
		log.info("Entered addScormCourse method");
		this.language.sendKeys(language);
		this.courseTitle.clear();
		this.courseTitle.sendKeys(courseTitle);
		this.grade.sendKeys(grade);
		this.semester.sendKeys(semester);
		this.syllabus.sendKeys(syllabus);
		this.subject.sendKeys(subject);
		this.country.sendKeys(country);
		this.metaInformation.clear();
		this.metaInformation.sendKeys(metaInformation);
		this.description.clear();
		this.description.sendKeys(description);
		this.availableFor.sendKeys(availableFor);
		this.status.sendKeys(status);
		
		this.uploadImage.sendKeys(uploadImage);
		waitForElementToLoad(this.imagePreview);
		
		this.addChapter.click();
		waitIfElementClickIsIntercepted(this.addLesson, "click", "");
		this.addLearningObject.click();
		waitForElementToLoad(this.maxLearningObject);
		this.maxLearningObject.click();
		waitIfElementClickIsIntercepted(this.individualLearningObject, "click", "");
		waitIfElementClickIsIntercepted(this.selectLearningObject, "click", "");
		waitForElementToLoad(exercizeQuestionsNum);
		exercizeQuestionsNum.sendKeys("2");
		this.quizTitle.sendKeys("title1");
		waitForElementToLoad(this.quizNumQuestions);
	
		
	}

	
	
	
	
	
}


