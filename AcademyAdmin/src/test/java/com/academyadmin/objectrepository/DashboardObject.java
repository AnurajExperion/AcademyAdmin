package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardObject {
	WebDriver driver;
	
	@FindBy(xpath="//h2[contains(text(),'Dashboard')]") 
	public WebElement dashboard;
	@FindBy(xpath="//a[contains(text(),'Users')]") 
	public WebElement users;
	@FindBy(xpath="//a[contains(text(),'Students list')]") 
	public WebElement studentList;
	@FindBy(xpath="//i[@class='fa fa-chevron-down']")
	public WebElement experionAdmin;
	@FindBy(xpath="//a[contains(text(),'Academy Admin')]")
	public WebElement academyAadmin;
	@FindBy(xpath="//a[contains(text(),'Logout')]")
	public WebElement logout;
	
	public DashboardObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
}
