package com.academyadmin.objectrepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class AddQuestionObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath = "//div[2]//div[1]//div[1]//div[1]//select[1]")
	public WebElement language;
	@FindBy(xpath = "//div[@class='card-body']//div[1]//div[2]//div[1]//select[1]")
	public WebElement subject;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//div[1]//select[1]")
	public WebElement topic;
	@FindBy(xpath = "//label[contains(text(),'Type')]//following::select[1]")
	public WebElement type;
	@FindBy(xpath = "//label[contains(text(),'Status')]//following::select[1]")
	public WebElement status;
	@FindBy(xpath = "//label[contains(text(),'Display Probability')]//following::input[1]")
	public WebElement displayProbability;
	@FindBy(xpath = "//input[@class='form-control ng-untouched ng-pristine ng-valid']")
	public WebElement questionNumber;
	@FindBy(xpath = "//label[contains(text(),'Where To Appear')]//following::span[1]")
	public WebElement examCheckBox;
	@FindBy(xpath = "//label[contains(text(),'Where To Appear')]//following::span[2]")
	public WebElement testCheckBox;
	@FindBy(xpath = "//label[contains(text(),'Where To Appear')]//following::span[3]")
	public WebElement quizCheckBox;
	@FindBy(xpath = "//label[contains(text(),'Where To Appear')]//following::span[4]")
	public WebElement exerciseCheckBox;
	@FindBy(xpath = "//label[contains(text(),'Where To Appear')]//following::span[5]")
	public WebElement pastExamCheckBox;	
	@FindBy(xpath = "//body[1]/app-root[1]/main[1]/app-question-add[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[5]/div[1]/div[1]/quill-editor[1]/div[2]/div[1]")
	public WebElement enterQuestion;	
	
	@FindBy(xpath = "//label[contains(text(),'Option 1')]//following::img[1]")
	public WebElement image1Preview;	
	@FindBy(xpath = "//label[contains(text(),'Option 2')]//following::img[1]")
	public WebElement image2Preview;	
	@FindBy(xpath = "//label[contains(text(),'Option 3')]//following::img[1]")
	public WebElement image3Preview;	
	@FindBy(xpath = "//label[contains(text(),'Option 4')]//following::img[1]")
	public WebElement image4Preview;
	@FindBy(xpath = "//label[contains(text(),'Option 5')]//following::img[1]")
	public WebElement image5Preview;
	@FindBy(xpath = "//label[contains(text(),'Option 6')]//following::img[1]")
	public WebElement image6Preview;
	
	@FindBy(xpath = "//span[contains(text(),'add new option')]")
	public WebElement addNewOption;	
	@FindBy(xpath = "//label[contains(text(),'Correct Answer Option')]//following::select[1]")
	public WebElement correctAnswerOption;
	@FindBy(xpath = "//div[@class='app-l-card__block']//div[2]//div[1]//div[1]//quill-editor[1]//div[2]//div[1]")
	public WebElement detailedExplanation;	
	@FindBy(xpath = "//button[contains(text(),'Add')]")
	public WebElement add;
	@FindBy(xpath = "//button[@class='btn btn-secondary app-u-mr']")
	public WebElement cancel;	
	@FindBy(xpath = "//span[contains(text(),'Subject is required.')]")
	public WebElement subjectMandatoryMsg ;
	@FindBy(xpath = "//span[contains(text(),'Topic is required.')]")
	public WebElement topicMandatoryMsg ;
	@FindBy(xpath = "//span[@class='help-block error ng-star-inserted']")
	public WebElement whereToAppearMandatoryMsg ;
	@FindBy(xpath = "//span[contains(text(),'Question is required.')]")
	public WebElement questionMandatoryMsg;
	@FindBy(xpath = "//div[@class='app-l-card__block ng-untouched ng-pristine ng-invalid']//div[1]//div[1]//div[1]//span[1]")
	public WebElement option1MandatoryMsg;
	@FindBy(xpath = "//div[@class='app-l-card__block ng-untouched ng-pristine ng-invalid']//div[2]//div[1]//div[1]//span[1]")
	public WebElement option2MandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Correct Answer is required.')]")
	public WebElement correctAnswerMandatoryMsg;
	@FindBy(xpath = "//span[contains(text(),'Detailed Explanation is required.')]")
	public WebElement explanationMandatoryMsg;	
	@FindBy(xpath = "//div[contains(text(),'Question has been created')]")	
	public WebElement questionCreateMsg;
	@FindBy(xpath = "//button[@class='btn btn-primary']")	
	public WebElement update;	
	
	public AddQuestionObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void questionAdd(String language, String subject, String topic, String type, String status, String displayProbability, String questionNumber, 
			Boolean examCheckBox, Boolean testCheckBox,	Boolean quizCheckBox, Boolean exerciseCheckBox, Boolean pastExamCheckBox, String enterQuestion,
			String option1, String uploadImage1, String correctAnswerOption, String detailedExplanation) throws InterruptedException {
		log.info("Entered questionAdd method");
		this.language.sendKeys(language);
		this.subject.sendKeys(subject);
		Thread.sleep(2000);
		waitIfElementClickIsIntercepted(this.topic, "select", topic);
		this.type.sendKeys(type);
		this.status.sendKeys(status);	
		Thread.sleep(2000);
		this.displayProbability.clear();
		this.displayProbability.sendKeys(displayProbability);
		this.questionNumber.clear();
		this.questionNumber.sendKeys(questionNumber);
		if(examCheckBox)
			this.examCheckBox.click();
		if(testCheckBox)
			this.testCheckBox.click();
		if(quizCheckBox)
			this.quizCheckBox.click();
		if(exerciseCheckBox)
			this.exerciseCheckBox.click();
		if(pastExamCheckBox)
			this.pastExamCheckBox.click();
		this.enterQuestion.clear();
		this.enterQuestion.sendKeys(enterQuestion);
		String[] optionSplit = option1.split(",");
		String[] imageSplit = uploadImage1.split(",");
		WebElement[] elements = {image1Preview,image2Preview,image3Preview,image4Preview,image5Preview,image6Preview};
		
		for(int i=0,j=1;i<optionSplit.length;i++,j++) {
			if(j>2) {
				waitIfElementClickIsIntercepted(this.addNewOption, "click", topic);
			}		
			driver.findElement(By.xpath("//label[contains(text(),'Option "+j+"')]//following::input[1]")).clear();
			driver.findElement(By.xpath("//label[contains(text(),'Option "+j+"')]//following::input[1]")).sendKeys(optionSplit[i]);
			driver.findElement(By.xpath("//label[contains(text(),'Option "+j+"')]//following::button[1]/input")).sendKeys(imageSplit[i]);
			waitForElementToLoad(elements[i]);	
		}
		
		waitForElementToLoad(this.correctAnswerOption);
		this.correctAnswerOption.sendKeys(correctAnswerOption);
		this.detailedExplanation.sendKeys(detailedExplanation);
		waitIfElementClickIsIntercepted(this.add, "click", topic);
	}
	
	public void questionEdit(String type, String status, String displayProbability, String questionNumber, String enterQuestion,String option1, 
			String option2, String correctAnswerOption, String detailedExplanation) {
		log.info("Entered questionEdit method");
		this.type.sendKeys(type);
		this.status.sendKeys(status);
		this.displayProbability.clear();
		this.displayProbability.sendKeys(displayProbability);
		this.enterQuestion.clear();
		this.enterQuestion.sendKeys(enterQuestion);
		/*
		 * this.option1.clear(); this.option1.sendKeys(option1); this.option2.clear();
		 * this.option2.sendKeys(option2);
		 */
		this.correctAnswerOption.sendKeys(correctAnswerOption);
		this.detailedExplanation.sendKeys(detailedExplanation);
		this.update.click();		
	}
	
	
}

