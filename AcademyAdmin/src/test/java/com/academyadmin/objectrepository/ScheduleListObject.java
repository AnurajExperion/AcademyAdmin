package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class ScheduleListObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath ="//tr[1]//td[8]//span[1]//tr[1]//td[1]//i[1]")
	public WebElement view;
	@FindBy(xpath ="//tr[2]//td[8]//span[1]//tr[1]//td[1]//i[1]")
	public WebElement edit;
	@FindBy(xpath ="//a[@class='btn btn-primary create-button']")
	public WebElement addNewSchedule ;
	@FindBy(xpath = "//div[@class='ui-toast-summary']")	
	public WebElement toastMsg;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Live session schedule data added successfully')]")	
	public WebElement livesessionCreateMsg;
	@FindBy(xpath = "//div[@class='ui-toast-summary'][contains(text(),'Live session schedule data edited successfully')]")	
	public WebElement livesessionEditMsg;
	
	
	
	public ScheduleListObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

}

