package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class StudentListObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath="//a[@class='btn btn-primary']") 	public WebElement addNewStudent;
	@FindBy(id="email") 	public WebElement emailSearch;
	@FindBy(xpath="//button[@class='btn btn-primary'][contains(text(),'Filter')]") 	public WebElement filter;
	@FindBy(xpath="//th[contains(text(),'E-mail')]/following::td[3]") 	public WebElement emailSearchResult;
	@FindBy(xpath="//a[contains(text(),'view details')]") 	public WebElement viewDetails;
	@FindBy(name="firstName") 	public WebElement firstName;
	@FindBy(xpath="//span[contains(text(),'First name')]/following::span[1]") 	public WebElement firstNameView;
	@FindBy(xpath="//label[contains(text(),'Last name')]/following::input[1]")	public WebElement lastName;
	@FindBy(xpath="//span[contains(text(),'Last name')]/following::span[1]")	public WebElement lastNameView;
	@FindBy(xpath="//div[3]//div[2]//input[1]") 	public WebElement email;
	@FindBy(xpath="//span[contains(text(),'Email')]/following::span[1]") 	public WebElement emailView;
	@FindBy(id="phone_prefix") 	public WebElement phonePrefix;
	@FindBy(id="phone_number")	public WebElement phoneNumber;
	@FindBy(xpath="//span[contains(text(),'Phone')]/following::span[1]")	public WebElement phoneNumberView;
	@FindBy(id="educationLevel") 	public WebElement grade;
	@FindBy(xpath="//span[contains(text(),'Grade')]/following::span[1]") 	public WebElement gradeView;
	@FindBy(xpath="//button[@class='btn btn-primary']") 	public WebElement createStudent;
	@FindBy(xpath="//button[contains(text(),'Back')]") 	public WebElement back;
	@FindBy(xpath="//a[contains(text(),'Edit user')]") 	public WebElement editStudent;
	@FindBy(xpath="//div[contains(text(),'New student has been added!')]") 	public WebElement addSuccessMsg;
	@FindBy(xpath="//label[contains(text(),'First name')]//following::div[2][contains(text(),'This field is required')]")
	public WebElement firstNameMandatoryMsg;
	@FindBy(xpath="//label[contains(text(),'First name')]//following::div[2][contains(text(),\"Provided value doesn't look like a user name\")]")
	public WebElement firstNameValidationMsg;
	@FindBy(xpath="//label[contains(text(),'Email')]//following::div[2][contains(text(),'This field is required')]")
	public WebElement emailMandatoryMsg;
	@FindBy(xpath="//label[contains(text(),'Email')]//following::div[2][contains(text(),\"Provided value doesn't look like an email\")]")
	public WebElement emailValidationMsg;
	@FindBy(xpath="//label[contains(text(),'Phone number')]//following::div[5][contains(text(),'This field is required')]")
	public WebElement phoneNumberMandatoryMsg;
	@FindBy(xpath="//label[contains(text(),'Grade')]//following::div[2][contains(text(),'This field is required')]")
	public WebElement gradeMandatoryMsg;
	@FindBy(xpath="//h2[contains(text(),'Search results:')]")
	public WebElement searchResult;
	@FindBy(xpath="//th[contains(text(),'OTP')]/following::td[1]")
	public WebElement otp;
	@FindBy(xpath="//button[contains(text(),'Reset password')]")
	public WebElement resetPassword;
	@FindBy(xpath="//button[contains(text(),'Submit')]")
	public WebElement resetPasswordSubmit;

		
	public StudentListObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void addEditStudent(String firstName, String lastName, String email, String phonePrefix, String phoneNumber, String grade) {
		log.info("Entered addEditStudent method");
		this.firstName.clear();
		this.firstName.sendKeys(firstName);
		this.lastName.clear();
		this.lastName.sendKeys(lastName);
		this.email.clear();
		this.email.sendKeys(email);
		this.phonePrefix.sendKeys(phonePrefix);
		this.phoneNumber.clear();
		this.phoneNumber.sendKeys(phoneNumber);
		this.grade.sendKeys(grade);
		this.createStudent.click();
	}
	
	public void mandatoryFieldsValidation(String firstName, String email, String phonePrefix, String phoneNumber, String grade) {
		log.info("Entered mandatoryFieldsValidation method");
		this.firstName.clear();
		this.firstName.sendKeys(firstName);
		this.email.clear();
		this.email.sendKeys(email);
		this.phonePrefix.sendKeys(phonePrefix);
		this.phoneNumber.clear();
		this.phoneNumber.sendKeys(phoneNumber);
		this.grade.sendKeys(grade);
	}

}
