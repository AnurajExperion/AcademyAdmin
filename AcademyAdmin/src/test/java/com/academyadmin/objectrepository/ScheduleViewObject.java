package com.academyadmin.objectrepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.academyadmin.baseclass.SetUp;

public class ScheduleViewObject extends SetUp {
	WebDriver driver;
	
	@FindBy(xpath = "//h5[@class='card-title']")
	public WebElement viewSchedule ;
	@FindBy(xpath = "//h5[contains(text(),'Title in English')]//following::span[1]")
	public WebElement titleEnglish;
	@FindBy(xpath = "//h5[contains(text(),'Title in Arabic')]//following::span[1]")
	public WebElement titleArabic;
	@FindBy(xpath = "//h5[contains(text(),'Schedule Image for English')]//following::img[1]")
	public WebElement imageForEnglish;
	@FindBy(xpath = "//h5[contains(text(),'Schedule Image for Arabic')]//following::img[1]")
	public WebElement imageForArabic;
	@FindBy(xpath = "//h5[contains(text(),'Country')]//following::span[1]")
	public WebElement country;
	@FindBy(xpath = "//h5[contains(text(),'Grade')]//following::span[1]")
	public WebElement grade;
	@FindBy(xpath = "//h5[contains(text(),'Live Stream URL')]//following::span[1]")
	public WebElement url;
	@FindBy(xpath = "//h5[contains(text(),'Status')]//following::span[1]")
	public WebElement status;
	@FindBy(xpath = "//a[@class='btn btn-primary edit-button']")
	public WebElement edit;
	@FindBy(xpath = "//button[@class='btn btn-secondary']")
	public WebElement back;
	
	
	public ScheduleViewObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
}
