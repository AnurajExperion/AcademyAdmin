package com.academyadmin.testscript;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.academyadmin.baseclass.SetUp;
import com.academyadmin.objectrepository.AcademyDashboardObject;
import com.academyadmin.objectrepository.AddLearningObjectObject;
import com.academyadmin.objectrepository.DashboardObject;
import com.academyadmin.objectrepository.SignInObject;
import com.academyadmin.testdata.AddLearningObjectData;
import com.academyadmin.testdata.TestDataImport;
import com.aventstack.extentreports.Status;

public class AddLearningObject extends SetUp {
	SignInObject sigInObj;
	DashboardObject dashboardObj;
	AcademyDashboardObject acadmeyDashboardObj;
	AddLearningObjectObject addLearningObj;
	AddLearningObjectData addLearningObjData;
	TestDataImport tdImport;
	String[] testData;
	
	@Test(priority = 0)
	public void verifyAddLearningObject() {
		try {
			log.info("AddLearningObject: verifyAddLearningObject");
			actualString=""; expectedString="";
			eTest = eReports.createTest("verifyAddLearningObject");
			eTest.assignCategory("AddLearningObject");
			expectedArray.add("Learning object added successfully.");
			addLearningObjData.getAddLearningObectData();
			
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addLearningObjects, "click", "");
			waitForElementToLoad(addLearningObj.language);
			addLearningObj.addLearningObject(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5], testData[6], 
					Boolean.parseBoolean(testData[7]), testData[8], testData[9]);
			
			waitForElementToLoad(addLearningObj.learningObjectCreateMsg);	
			actualArray.add(addLearningObj.learningObjectCreateMsg.getText());			
		}catch (Exception e) {			
			System.out.println("verifyAddLearningObject Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("Actual: "+actualArray+"\nExpcted: "+expectedArray);
		Assert.assertEquals(actualArray , expectedArray);	
	}
	
	@BeforeClass
	public void initilaize() {
		try {
			log.info("AddLearningObject: initilaize");
			sigInObj = new SignInObject(driver);
			dashboardObj = new DashboardObject(driver);
			acadmeyDashboardObj = new AcademyDashboardObject(driver);
			addLearningObj = new AddLearningObjectObject(driver);
			addLearningObjData = new AddLearningObjectData();
			
			tdImport = new TestDataImport();
			tdImport.readExcel("StudentList");
			if(!suiteName.contains("Failed suite"))
				addLearningObjData.generateFakeData();
			
			waitForElementToLoad(sigInObj.email);
			sigInObj.signIn("automation@mailinator.com",  "Strongpassword1");
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			dashboardObj.academyAadmin.click();	
			chromeTab = new ArrayList<>(driver.getWindowHandles());
			driver.switchTo().window(chromeTab.get(1));
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
	
	@AfterClass
	public void logout() {
		try {
			log.info("AddQuestion: logout");
			driver.switchTo().window(chromeTab.get(0));
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			waitIfElementClickIsIntercepted(dashboardObj.logout, "click", "");
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
}
