package com.academyadmin.testscript;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.academyadmin.baseclass.SetUp;
import com.academyadmin.objectrepository.AcademyDashboardObject;
import com.academyadmin.objectrepository.AddScormCourseObject;
import com.academyadmin.objectrepository.DashboardObject;
import com.academyadmin.objectrepository.HomeScreenObjectAndroid;
import com.academyadmin.objectrepository.LibraryObjectAndroid;
import com.academyadmin.objectrepository.OnboradingObjectAndroid;
import com.academyadmin.objectrepository.ScormCourseListObject;
import com.academyadmin.objectrepository.ScormCourseViewObject;
import com.academyadmin.objectrepository.SignInObject;
import com.academyadmin.objectrepository.SignInObjectAndroid;
import com.academyadmin.testdata.ScormCourseData;
import com.academyadmin.testdata.TestDataImport;
import com.aventstack.extentreports.Status;

public class AddScormCourse extends SetUp {
	TestDataImport tdImport;
	SignInObject sigInObj;
	DashboardObject dashboardObj;
	AcademyDashboardObject acadmeyDashboardObj;
	AddScormCourseObject scormObj;
	ScormCourseListObject scormListObj;
	ScormCourseViewObject scormViewObj;
	ScormCourseData scormDataObj;
	SignInObjectAndroid signInObjAnd;
	OnboradingObjectAndroid onboardingObjAnd;
	HomeScreenObjectAndroid homeObjAnd;
	LibraryObjectAndroid libraryObjAnd;
	
	String split="";
	String[] testData;

	@Test(priority = 0)
	public void verifyScormCourseAdd() {
		try {
			log.info("Entered verifyScormCourseAdd mehtod");
			actualArray = new ArrayList<String>(); expectedArray=new ArrayList<>();
			eTest = eReports.createTest("verifyScormCourseAdd");
			eTest.assignCategory("ScormCourse");
			testData = scormDataObj.getAddScormData();
			expectedArray.add("Course created successfully");
			for(int i=0;i<9;i++) {
				if(i!=7)//meta information is not displayed in view
					expectedArray.add(testData[i]);
			}
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addScormCourse, "click", "");
			waitForElementToLoad(scormObj.language);
			scormObj.addScormCourse(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5], testData[6], testData[7], 
					testData[8], testData[9], testData[10]);
			waitForElementToLoad(scormListObj.courseCreateMsg);	
			actualArray.add(scormListObj.courseCreateMsg.getText());
			
			waitForElementToLoad(scormListObj.selectGrade);
			scormListObj.scormSearch(expectedArray.get(3), expectedArray.get(4), expectedArray.get(6), expectedArray.get(2));
			waitIfElementClickIsIntercepted(scormListObj.view, "click", "");
			
			waitForElementToLoad(scormViewObj.language);
			split = scormViewObj.language.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.title.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.grade.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.semester.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.syllabus.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.subject.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.country.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());			
			split = scormViewObj.description.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());			
		}catch (Exception e) {
			System.out.println("verifyScormCourseAdd Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("verifyScormCourseAdd\nActual: "+actualArray+"\nExpcted: "+expectedArray);
		Assert.assertEquals(actualArray, expectedArray);	
	}
	
	@Test(priority = 1, enabled = false)
	public void verifyScormOnApp() {
		try {
			log.info("Entered verifyScormOnApp method");
			actualString=""; expectedString="";
			eTest = eReports.createTest("verifyScormOnApp");
			eTest.assignCategory("ScormCourse");
			expectedString = expectedArray.get(1);
			
			waitForElementToLoad(onboardingObjAnd.login);
			onboardingObjAnd.login.click();			
			
			waitForElementToLoad(signInObjAnd.email);	
			signInObjAnd.androidLogin(email,password);
			waitForElementToLoad(homeObjAnd.library);
			homeObjAnd.library.click();
			waitForElementToLoad(libraryObjAnd.english);
			libraryObjAnd.english.click();
			waitForElementToLoad(libraryObjAnd.scormCourseName);
			actualString = libraryObjAnd.scormCourseName.getText();
		}catch (Exception e) {
			System.out.println("verifyScormOnApp Failed"+actualString);
			e.printStackTrace();
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("verifyScormOnApp\nActual: "+actualString+"\nExpcted: "+expectedString);
		Assert.assertEquals(actualString, expectedString);
	}
	
	@Test(priority = 2)
	public void verifyValidScorm() {		
		try {
			log.info("Entered verifyValidScorm method");
			actualString=""; expectedString="";
			eTest = eReports.createTest("verifyScormStatus");
			eTest.assignCategory("ScormCourse");
			expectedString = "Success";
			testData = scormDataObj.getValidScormData();
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addScormCourse, "click", "");
			waitForElementToLoad(scormObj.language);
			scormObj.addScormCourse(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5], testData[6], testData[7], 
					testData[8], testData[9], testData[10]);
			
			waitForElementToLoad(scormListObj.selectGrade);
			for(int i=0;i<10;i++) {
				scormListObj.scormSearch(testData[2], testData[3], testData[5], testData[1]);
				actualString = staleExceptionOnMessage(scormListObj.scormStatus);
				if(actualString.equals(expectedString)||actualString.equals("Failed"))
					break;
				waitIfElementClickIsIntercepted(scormListObj.clear, "click", "");
			}
		}catch (Exception e) {
			System.out.println("verifyScormStatus Failed\n"+actualString);
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}
		System.out.println("verifyValidScorm\nActual: "+actualString+"\nExpcted: "+expectedString);
		Assert.assertEquals(actualString, expectedString);		
	}
	
	@Test(priority = 3)
	public void verifyInValidScorm(){
		try {
			log.info("Entered verifyInValidScorm method");
			actualString=""; expectedString="";
			eTest = eReports.createTest("verifyInValidScorm");
			eTest.assignCategory("ScormCourse");
			expectedString ="Failed";
			testData = scormDataObj.getInvalidScormData();
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addScormCourse, "click", "");
			waitForElementToLoad(scormObj.language);
			scormObj.addScormCourse(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5], testData[6], testData[7], 
					testData[8], testData[9], testData[10]);
			
			waitForElementToLoad(scormListObj.selectGrade);			
			for(int i=0;i<10;i++) {				
				scormListObj.scormSearch(testData[2], testData[3], testData[5], testData[1]);
				actualString = staleExceptionOnMessage(scormListObj.scormStatus);
				if(actualString.equals(expectedString)||actualString.equals("Failed"))
					break;
				waitIfElementClickIsIntercepted(scormListObj.clear, "click", "");
			}			
		}
		catch(Exception e) {
			System.out.println("verifyInvalidScorm Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}
		System.out.println("verifyInValidScorm\nActual: "+actualString+"\nExpcted: "+expectedString);
		Assert.assertEquals(actualString, expectedString);	
	}
	
	//@Test(priority = 4)
	public void verifyFieldsInEdit() {
		try {
			log.info("Entered verifyFieldsInEdit method");			
			actualBoolArray = new ArrayList<>();
			expectedBoolArray = new ArrayList<>();
			actualString=""; expectedString="";
			eTest = eReports.createTest("verifyFieldsInEdit");
			eTest.assignCategory("ScormCourse");
			expectedString ="Success";
			testData = scormDataObj.getFieldStatusWhileEditData();
			
			expectedBoolArray.add(false); // field disabled
			expectedBoolArray.add(true);  // field enabled
			expectedBoolArray.add(false); // field disabled
			expectedBoolArray.add(false); // field disabled
			expectedBoolArray.add(false); // field disabled
			expectedBoolArray.add(false); // field disabled
			expectedBoolArray.add(false); // field disabled
			expectedBoolArray.add(true);  // field enabled
			expectedBoolArray.add(false); // field disabled
			expectedBoolArray.add(true);  // field enabled
			expectedBoolArray.add(true);  // field enabled			
			
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addScormCourse, "click", "");
			waitForElementToLoad(scormObj.language);
			scormObj.addScormCourse(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5], testData[6], testData[7], 
					testData[8], testData[9], testData[10]);
			
			waitForElementToLoad(scormListObj.selectGrade);	
			for(int i=0;i<10;i++) {				
				scormListObj.scormSearch(testData[2], testData[3], testData[5], testData[1]);
				actualString = staleExceptionOnMessage(scormListObj.scormStatus);
				if(actualString.equals(expectedString)||actualString.equals("Success"))
					break;
				waitIfElementClickIsIntercepted(scormListObj.clear, "click", "");
			}
			
			waitIfElementClickIsIntercepted(scormListObj.edit, "click", "");
			waitForElementToLoad(scormObj.language);
			Thread.sleep(2000); //elements are taking time to get disabled
			actualBoolArray.add(isElementEnabled(scormObj.language));
			actualBoolArray.add(isElementEnabled(scormObj.courseTitle));
			actualBoolArray.add(isElementEnabled(scormObj.grade));
			actualBoolArray.add(isElementEnabled(scormObj.semester));
			actualBoolArray.add(isElementEnabled(scormObj.syllabus));
			actualBoolArray.add(isElementEnabled(scormObj.subject));
			actualBoolArray.add(isElementEnabled(scormObj.country));
			actualBoolArray.add(isElementEnabled(scormObj.status));
			actualBoolArray.add(isElementEnabled(scormObj.metaInformation));
			actualBoolArray.add(isElementEnabled(scormObj.description));
			actualBoolArray.add(isElementEnabled(scormObj.uploadImage));			
		}
		catch(Exception e) {
			System.out.println("verifyFieldsInEdit Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}
		System.out.println("verifyFieldsInEdit\nActual: "+actualBoolArray+"\nExpcted: "+expectedBoolArray);
		Assert.assertEquals(actualBoolArray, expectedBoolArray);
	}
		
	@Test(priority = 5)
	public void verifyEditBeforeSuccess() {
		try {
			log.info("Entered verifyEdit method");
			actualArray = new ArrayList<>();
			expectedArray = new ArrayList<>();
			actualString=""; expectedString="Course updated successfully";
			eTest = eReports.createTest("verifyEdit");
			eTest.assignCategory("ScormCourse");
			testData = scormDataObj.getEditBeforeSuccessData();
			expectedArray.add("Course updated successfully");
			for(int i=0;i<9;i++) {
				if(i!=7)//meta information is not displayed in view
					expectedArray.add(testData[i]);
			}
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addScormCourse, "click", "");
			waitForElementToLoad(scormObj.language);
			scormObj.addScormCourse(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5], testData[6], testData[7], 
					testData[8], testData[9], testData[10]);
			
			waitForElementToLoad(scormListObj.selectGrade);
			scormListObj.scormSearch(testData[2], testData[3], testData[5], testData[1]);			
			waitIfElementClickIsIntercepted(scormListObj.edit, "click", "");
			waitForElementToLoad(scormObj.courseTitle);
			
			scormObj.editScormCourse(testData[11], testData[12], testData[13], testData[14], testData[15], testData[16], testData[17], testData[18], 
					testData[19]);
			
			waitForElementToLoad(scormListObj.courseEditMsg);
			actualArray.add(scormListObj.courseEditMsg.getText());
			actualString = scormListObj.courseEditMsg.getText();
			
			waitForElementToLoad(scormListObj.selectGrade);
			scormListObj.scormSearch(testData[13], testData[14], testData[16], testData[12]);			
			waitIfElementClickIsIntercepted(scormListObj.view, "click", "");
			
			waitForElementToLoad(scormViewObj.title);
			expectedArray.set(2,testData[0]);
			expectedArray.set(8,testData[1]);
			split = scormViewObj.language.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.title.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.grade.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.semester.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.syllabus.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.subject.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());
			split = scormViewObj.country.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());			
			split = scormViewObj.description.getText();
			actualArray.add(split.substring(split.lastIndexOf(":")+1).trim());			
		}
		catch (Exception e) {
			System.out.println("verifyEdit Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}
		System.out.println("verifyEdit\nActual: "+actualArray+"\nExpcted: "+expectedArray);
		Assert.assertEquals(actualArray, expectedArray);
	}
	
	@Test(priority = 6)
	public void verifyDelete() {
		try {
			log.info("Entered verifyDelete method");
			actualArray = new ArrayList<String>(); 
			expectedArray = new ArrayList<String>();
			eTest = eReports.createTest("verifyDelete");
			eTest.assignCategory("ScormCourse");
			testData = scormDataObj.getScormDeleteData();
			expectedArray.add("SCORM course was deleted successfully.");
			expectedArray.add("No records found");
			
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addScormCourse, "click", "");
			waitForElementToLoad(scormObj.language);
			//Thread.sleep(2000);
			scormObj.addScormCourse(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5], testData[6], testData[7], 
					testData[8], testData[9], testData[10]);
			
			waitForElementToLoad(scormListObj.selectGrade);
			scormListObj.scormSearch(testData[2], testData[3], testData[5], testData[1]);
			waitIfElementClickIsIntercepted(scormListObj.delete, "click", "");
			waitForElementToLoad(scormListObj.deleteYes);
			scormListObj.deleteYes.click();
			waitForElementToLoad(scormListObj.courseDeleteMsg);
			actualArray.add(scormListObj.courseDeleteMsg.getText());
			
			waitForElementToLoad(scormListObj.selectGrade);
			scormListObj.scormSearch(testData[2], testData[3], testData[5], testData[1]);
			waitForElementToLoad(scormListObj.noRecordsFound);
			actualArray.add(scormListObj.noRecordsFound.getText());
			
		}catch (Exception e) {
			System.out.println("verifyDelete Failed");
			e.printStackTrace();			
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("verifyDelete\nActual: "+actualArray+"\nExpcted: "+expectedArray);
		Assert.assertEquals(actualArray, expectedArray);
	}
	
	@Test(priority = 7)
	public void verifyMandatoryMsgVisible() {
		try {
			log.info("Entered verifyMandatoryMsgVisible method in AddScorm");
			actualBoolArray = new ArrayList<Boolean>(); 
			expectedBoolArray = new ArrayList<Boolean>();
			eTest = eReports.createTest("verifyMandatoryMsgVisible of AddScorm");
			eTest.assignCategory("AddScormCourse");
			
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(true);//warning message present
			 			
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addScormCourse, "click", "");
			waitIfElementClickIsIntercepted(scormObj.addScormCourse, "click", "");			
			waitForElementToLoad(scormObj.languageMandatoryMsg);
			
			actualBoolArray.add(isElementPresent(scormObj.languageMandatoryMsg));		
			actualBoolArray.add(isElementPresent(scormObj.titleMandatoryMsg));			
			actualBoolArray.add(isElementPresent(scormObj.gradeMandatoryMsg));			
			actualBoolArray.add(isElementPresent(scormObj.semesterMandatoryMsg));
			actualBoolArray.add(isElementPresent(scormObj.syllabusMandatoryMsg));
			actualBoolArray.add(isElementPresent(scormObj.subjectMandatoryMsg));
			actualBoolArray.add(isElementPresent(scormObj.countryMandatoryMsg));
			actualBoolArray.add(isElementPresent(scormObj.descriptionMandatoryMsg));
			actualBoolArray.add(isElementPresent(scormObj.imageMandatoryMsg));
			actualBoolArray.add(isElementPresent(scormObj.scormFileMandatoryMsg));		
		}		
		catch(Exception e) {
			System.out.println("verifyMandatoryMsgVisible Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);	
		}
		System.out.println("verifyMandatoryMsgVisible\nActual: "+actualBoolArray+"\nExpcted: "+expectedBoolArray);
		Assert.assertEquals(actualBoolArray,expectedBoolArray);	
	}
	
	@Test(priority = 8)
	public void verifyTitleMinLengthValidation() {		
		try {
			log.info("Entered verifyLengthValidation method");
			actualBoolArray = new ArrayList<Boolean>(); 
			expectedBoolArray = new ArrayList<Boolean>();
			eTest = eReports.createTest("verifyLengthValidation");
			eTest.assignCategory("AddScormCourse");
			expectedBoolArray.add(true);//warning message present 
			expectedBoolArray.add(true);//warning message present
			expectedBoolArray.add(false);//warning message not present
			expectedBoolArray.add(false);//warning message not present
			
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addScormCourse, "click", "");			
			waitForElementToLoad(scormObj.courseTitle);
			testData = scormDataObj.getTitleMinLengthValidationData();
			
			for(int i=0;i<4;i++) {				
				scormObj.titleLengthValidation(testData[i]);				
				scormObj.addScormCourse.click();
				actualBoolArray.add(isElementPresent(scormObj.titlelengthValidationMsg));				
			}			
		}
		catch(Exception e) {
			System.out.println("verifyLengthValidation Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}
		System.out.println("verifyLengthValidation\nActual: "+actualBoolArray+"\nExpected: "+expectedBoolArray);
		Assert.assertEquals(actualBoolArray,expectedBoolArray);
	}
	
	@BeforeClass
	public void initialize() {
		try {
			log.info("AddScormCourse: Entered initialize method");
			
			sigInObj = new SignInObject(driver);
			dashboardObj = new DashboardObject(driver);
			acadmeyDashboardObj = new AcademyDashboardObject(driver);
			scormObj = new AddScormCourseObject(driver);
			scormListObj = new ScormCourseListObject(driver);
			scormViewObj = new ScormCourseViewObject(driver);
			scormDataObj = new ScormCourseData();
			
			/*
			 * signInObjAnd = new SignInObjectAndroid(androidDriver); onboardingObjAnd = new
			 * OnboradingObjectAndroid(androidDriver); homeObjAnd = new
			 * HomeScreenObjectAndroid(androidDriver);
			 */
			
			tdImport = new TestDataImport();
			tdImport.readExcel("AddScormCourse");
			if(!suiteName.contains("Failed suite"))
				scormDataObj.generateFakeData();
			
			waitForElementToLoad(sigInObj.email);
			sigInObj.signIn("automation@mailinator.com",  "Strongpassword1");
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			dashboardObj.academyAadmin.click();	
			chromeTab = new ArrayList<>(driver.getWindowHandles());
			driver.switchTo().window(chromeTab.get(1));
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("initialize Failed" + e);
			log.info(e);
		}		
	}
	
	@AfterClass
	public void logout() {
		try {
			log.info("AddScormCourse: logout");
			driver.switchTo().window(chromeTab.get(0));
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			waitIfElementClickIsIntercepted(dashboardObj.logout, "click", "");
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
}
