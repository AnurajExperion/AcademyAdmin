package com.academyadmin.testscript;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.academyadmin.baseclass.SetUp;
import com.academyadmin.objectrepository.AcademyDashboardObject;
import com.academyadmin.objectrepository.AddCourseObject;
import com.academyadmin.objectrepository.DashboardObject;
import com.academyadmin.objectrepository.SignInObject;
import com.academyadmin.testdata.AddCourseData;
import com.academyadmin.testdata.TestDataImport;
import com.aventstack.extentreports.Status;

public class AddCourse extends SetUp {
	SignInObject sigInObj;
	AddCourseObject addCourseObj;
	AddCourseData addCourseDataObj;
	DashboardObject dashboardObj;
	AcademyDashboardObject acadmeyDashboardObj;
	TestDataImport tdImport;
	String[] testData;
	
	@Test(priority = 0)
	public void verifyAddCourse() {
		try {
		log.info("AddCourse: verifyAddCourse");
		actualString=""; expectedString="";
		eTest = eReports.createTest("verifyAddCourse");
		eTest.assignCategory("Add Course");
		expectedString = "course has been created";
		addCourseDataObj.getAddCourseData();
		
		waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
		waitIfElementClickIsIntercepted(acadmeyDashboardObj.addCourse, "click", "");
		waitForElementToLoad(addCourseObj.language);
		waitForElementToLoad(addCourseObj.courseTitle);
		addCourseObj.courseAdd(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5], testData[6], testData[7], 
				testData[8], testData[9], testData[10],testData[11]);
		actualString = "course has been created";
		}
		
		catch (Exception e) {			
			System.out.println("verifyAddCourse Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("Actual: "+actualString+"\nExpcted: "+expectedString);
		Assert.assertEquals(actualString , expectedString);
	}	
	
	@BeforeClass
	public void initilaize() {
		try {
			log.info("AddQuestion: initilaize");
			sigInObj = new SignInObject(driver);
			addCourseObj = new AddCourseObject(driver);
			addCourseDataObj = new AddCourseData();
			dashboardObj = new DashboardObject(driver);
			acadmeyDashboardObj = new AcademyDashboardObject(driver);
			
			tdImport = new TestDataImport();
			tdImport.readExcel("StudentList");
			if(!suiteName.contains("Failed suite"))
				addCourseDataObj.generateFakeData();
			
			waitForElementToLoad(sigInObj.email);
			sigInObj.signIn("automation@mailinator.com",  "Strongpassword1");
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			dashboardObj.academyAadmin.click();	
			chromeTab = new ArrayList<>(driver.getWindowHandles());
			driver.switchTo().window(chromeTab.get(1));
		} catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}
	}
	
	@AfterClass
	public void logout() {
		try {
			log.info("AddQuestion: logout");
			driver.switchTo().window(chromeTab.get(0));
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			waitIfElementClickIsIntercepted(dashboardObj.logout, "click", "");
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
}
