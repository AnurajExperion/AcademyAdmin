package com.academyadmin.testscript;

import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.academyadmin.baseclass.SetUp;
import com.academyadmin.objectrepository.DashboardObject;
import com.academyadmin.objectrepository.HomeScreenObjectAndroid;
import com.academyadmin.objectrepository.OnboradingObjectAndroid;
import com.academyadmin.objectrepository.ProfileObjectAndroid;
import com.academyadmin.objectrepository.SignInObject;
import com.academyadmin.objectrepository.SignInObjectAndroid;
import com.academyadmin.objectrepository.StudentListObject;
import com.academyadmin.testdata.StudentListData;
import com.academyadmin.testdata.TestDataImport;
import com.aventstack.extentreports.Status;

public class StudentList extends SetUp {
	SignInObject sigInObj;
	StudentListObject studObj;
	DashboardObject dashboardObj;
	StudentListData studDataObj;
	SignInObjectAndroid signInObjAnd;
	OnboradingObjectAndroid onboardingObjAnd;
	HomeScreenObjectAndroid homeObjAnd;
	ProfileObjectAndroid profileObjAnd;
	TestDataImport tdImport;
	
	String getTextboxData="";
	String[] fullName = new String[2];
	String[] testData;
	
	@Test(priority = 0)
	public void verifySignup() {
		try {
			log.info("Entered verifySignup method");
			actualString = "";
			actualArray = new ArrayList<>(); expectedArray = new ArrayList<String>();
			eTest = eReports.createTest("verifySignup");
			eTest.assignCategory("StudentList");
			testData = studDataObj.getAddData();
			expectedArray.add("New student has been added!");
			expectedArray.add(testData[0]);//first name
			expectedArray.add(testData[1]);//last name
			expectedArray.add(testData[2]);//email
			expectedArray.add(testData[3] + " " + testData[4]);//phone number
			expectedArray.add(testData[5]);//grade
			
			email = testData[2];
			password = "123456";
			
			waitForElementToLoad(dashboardObj.studentList);
			dashboardObj.studentList.click();
			waitIfElementClickIsIntercepted(studObj.addNewStudent, "click", "");
			waitForElementToLoad(studObj.firstName);
			studObj.addEditStudent(testData[0],testData[1],testData[2],testData[3],testData[4],testData[5]);
						
			actualString = staleExceptionOnMessage(studObj.addSuccessMsg);
			actualArray.add(actualString);
			
			waitForElementToLoad(studObj.emailSearch);
			studObj.emailSearch.sendKeys(expectedArray.get(3));
			studObj.filter.click();
			Thread.sleep(2000);
			waitForElementToLoad(studObj.viewDetails);
			studObj.viewDetails.click();
			waitForElementToLoad(studObj.firstNameView);
			actualArray.add(studObj.firstNameView.getText());
			actualArray.add(studObj.lastNameView.getText());
			actualArray.add(studObj.emailView.getText());
			actualArray.add(studObj.phoneNumberView.getText());
			actualArray.add(studObj.gradeView.getText());	
			
		}catch (Exception e) {
			System.out.println("verifySignup Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("verifySignup\nActual: "+actualArray+"\nExpcted: "+expectedArray);
		Assert.assertEquals(actualArray,expectedArray);		
	}
	
	@Test(priority = 1, enabled = true) 
	public void verifyUserOnApp() {
		try {
			log.info("Entered verifyUserOnApp method");
			actualArray = new ArrayList<>();
			eTest = eReports.createTest("verifyUserOnApp");
			eTest.assignCategory("StudentList");
			
			waitForElementToLoad(dashboardObj.studentList);
			dashboardObj.studentList.click(); waitForElementToLoad(studObj.emailSearch);
			studObj.emailSearch.sendKeys(email); studObj.filter.click();
			Thread.sleep(2000); waitForElementToLoad(studObj.viewDetails);
			studObj.viewDetails.click(); waitForElementToLoad(studObj.firstNameView);
			 
			studObj.resetPassword.click();
			waitForElementToLoad(studObj.resetPasswordSubmit);
			  
			studObj.resetPasswordSubmit.click(); waitForElementToLoad(studObj.otp);
			String otp= studObj.otp.getText(); ((JavascriptExecutor)
			driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			  
			waitForElementToLoad(onboardingObjAnd.login);
			onboardingObjAnd.login.click();
			waitForElementToLoad(signInObjAnd.email);		
			signInObjAnd.androidLogin(email, otp);
			clickIfElementIsPresent(signInObjAnd.alreadyLoggedInYes);
			
			waitForElementToLoad(homeObjAnd.password);
			homeObjAnd.password.sendKeys(otp);
			homeObjAnd.newPassword.sendKeys("123456");
			homeObjAnd.confirmPassword.sendKeys("123456");
			homeObjAnd.submitPassword.click();			
			
			Thread.sleep(3000);
			WebElement[] element= {homeObjAnd.welcomePopup1,homeObjAnd.welcomePopup2,homeObjAnd.welcomePopup3,homeObjAnd.welcomePopup4,homeObjAnd.familyMemberPopup};
			handlePopups(element);
			handlePopups(element);
				
			waitForElementToLoad(homeObjAnd.profile);
			homeObjAnd.profile.click();
			waitIfElementClickIsIntercepted(profileObjAnd.edit, "click", "");
			waitForElementToLoad(profileObjAnd.fullName);
			scrollUp(profileObjAnd.prefLanguage);
			profileObjAnd.prefLanguage.click();
			
			waitForElementToLoad(profileObjAnd.english);
			profileObjAnd.english.click();
			waitForElementToLoad(profileObjAnd.save);
			profileObjAnd.save.click();
			
			Thread.sleep(3000);
			handlePopups(element);
			
			waitForElementToLoad(homeObjAnd.profile);
			homeObjAnd.profile.click();
			waitIfElementClickIsIntercepted(profileObjAnd.edit, "click", "");			
			
			waitForElementToLoad(profileObjAnd.fullName);
			fullName=profileObjAnd.fullName.getText().split(" ");
			actualArray.add("New student has been added!");
			actualArray.add(fullName[0]);
			actualArray.add(fullName[1]);
			actualArray.add(profileObjAnd.email.getText());
			actualArray.add(profileObjAnd.prefix.getText()+" "+profileObjAnd.mobile.getText());
			actualArray.add(profileObjAnd.grade.getText());
			scrollUp(profileObjAnd.logout);
			profileObjAnd.logout.click();
		}catch (Exception e) {
			System.out.println("verifyUserOnApp Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}
		System.out.println("verifyUserOnApp\nActual: "+actualArray+"\nExpcted: "+expectedArray);
		Assert.assertEquals(actualArray,expectedArray);		
	}
	
	@Test(priority = 2)
	public void verifyEdit() {
		try {
			log.info("Entered verifyEdit method");
			actualArray = new ArrayList<>();
			expectedArray = new ArrayList<>();
			eTest = eReports.createTest("verifyEdit");
			eTest.assignCategory("StudentList");			
			
			testData = studDataObj.getEditData();//for add	
			
			waitForElementToLoad(dashboardObj.studentList);
			dashboardObj.studentList.click();
			
			waitIfElementClickIsIntercepted(studObj.addNewStudent, "click", "");
			waitForElementToLoad(studObj.firstName);
			studObj.addEditStudent(testData[0],testData[1],testData[2],testData[3],testData[4],testData[5]);			
			waitForElementToLoad(studObj.emailSearch);
			studObj.emailSearch.sendKeys(testData[2]);
			studObj.filter.click();
			Thread.sleep(2000);
			waitForElementToLoad(studObj.viewDetails);
			studObj.viewDetails.click();
			waitForElementToLoad(studObj.editStudent);
			studObj.editStudent.click();
			waitForElementToLoad(studObj.firstName);
			
			expectedArray.add(testData[6]);//first name
			expectedArray.add(testData[7]);//last name
			expectedArray.add(testData[8]);//email
			expectedArray.add(testData[9] + " " + testData[10]);//phone number
			expectedArray.add(testData[11]);//grade			
			
			studObj.addEditStudent(testData[6],testData[7],testData[8],testData[9],testData[10],testData[11]);
			waitForElementToLoad(studObj.firstNameView);
			actualArray.add(studObj.firstNameView.getText());
			actualArray.add(studObj.lastNameView.getText());
			actualArray.add(studObj.emailView.getText());
			actualArray.add(studObj.phoneNumberView.getText());
			actualArray.add(studObj.gradeView.getText());			
		}catch (Exception e) {
			System.out.println("verifyEdit Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("verifyEdit\nActual: "+actualArray+"\nExpected: "+expectedArray);
		Assert.assertEquals(actualArray,expectedArray);
	}
	
	@Test(priority = 3)
	public void verifyMandatoryMsgVisible() {		
		try {
			log.info("Entered verifyMandatoryMsgVisible method");
			actualArray = new ArrayList<>();
			expectedArray = new ArrayList<>();
			eTest = eReports.createTest("verifyMandatoryMsgVisible");
			eTest.assignCategory("StudentList");
			expectedArray.add("This field is required");//warning message present
			expectedArray.add("This field is required");//warning message present
			expectedArray.add("This field is required");//warning message present
			expectedArray.add("This field is required");//warning message present
			
			waitForElementToLoad(dashboardObj.studentList);
			dashboardObj.studentList.click();			
			waitIfElementClickIsIntercepted(studObj.addNewStudent, "click", "");
			waitForElementToLoad(studObj.firstName);
			actualArray.add(studObj.firstNameMandatoryMsg.getText());
			actualArray.add(studObj.emailMandatoryMsg.getText());
			actualArray.add(studObj.phoneNumberMandatoryMsg.getText());
			actualArray.add(studObj.gradeMandatoryMsg.getText());			
		}catch (Exception e) {
			System.out.println("verifyMandatoryMsgVisible Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
			Assert.assertEquals(actualArray,expectedArray);
		}
		System.out.println("verifyMandatoryMsgVisible\nActual: "+actualArray+"\nExpected: "+expectedArray);
		Assert.assertEquals(actualArray,expectedArray);
	}
	
	@Test(priority = 4)
	public void verifyMandatoryMsgDismiss() {		
		try {
			log.info("Entered verifyMandatoryMsgDismiss method");
			actualBoolArray = new ArrayList<>();
			expectedBoolArray = new ArrayList<>();
			eTest = eReports.createTest("verifyMandatoryMsgDismiss");
			eTest.assignCategory("StudentList");
			testData = studDataObj.getMandatoryMsgDismissData();
			expectedBoolArray.add(false);//warning message not present
			expectedBoolArray.add(false);//warning message not present
			expectedBoolArray.add(false);//warning message not present
			expectedBoolArray.add(false);//warning message not present
			
			waitForElementToLoad(dashboardObj.studentList);
			dashboardObj.studentList.click();
			waitIfElementClickIsIntercepted(studObj.addNewStudent, "click", "");
			waitForElementToLoad(studObj.firstName);			
			studObj.mandatoryFieldsValidation(testData[0], testData[1], testData[2], testData[3], testData[4]);			
			actualBoolArray.add(isElementPresent(studObj.firstNameMandatoryMsg));		
			actualBoolArray.add(isElementPresent(studObj.emailMandatoryMsg));			
			actualBoolArray.add(isElementPresent(studObj.phoneNumberMandatoryMsg));			
			actualBoolArray.add(isElementPresent(studObj.gradeMandatoryMsg));
		}catch (Exception e) {
			System.out.println("verifyMandatoryMsgDismiss Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}
		System.out.println("verifyMandatoryMsgDismiss\nActual: "+actualBoolArray+"\nExpected: "+expectedBoolArray);
		Assert.assertEquals(actualBoolArray,expectedBoolArray);		
	}
	
	@Test(priority = 5)
	public void verifyLengthValidation() {		
		try {
			log.info("Entered verifyLenghtValidation method");
			actualBoolArray = new ArrayList<>();
			expectedBoolArray = new ArrayList<>();
			eTest = eReports.createTest("verifyLengthValidation");
			eTest.assignCategory("StudentList");
			expectedBoolArray.add(true);expectedBoolArray.add(false);//warning message present & submit button inactive
			expectedBoolArray.add(false);expectedBoolArray.add(true);//warning message not present & submit button inactive
			expectedBoolArray.add(false);expectedBoolArray.add(true);//warning message not present & submit button active
			expectedBoolArray.add(false);expectedBoolArray.add(true);//warning message not present & submit button active
			expectedBoolArray.add(false);expectedBoolArray.add(true);//warning message not present & submit button active
			expectedBoolArray.add(false);expectedBoolArray.add(true);//warning message not present & submit button active
			
			for(int i=0;i<6;i++) {
				testData = studDataObj.getLengthValidationData();			
				waitForElementToLoad(dashboardObj.studentList);
				dashboardObj.studentList.click();				
				waitIfElementClickIsIntercepted(studObj.addNewStudent, "click", "");				
				waitForElementToLoad(studObj.back);
				waitForElementToLoad(studObj.firstName);
				studObj.mandatoryFieldsValidation(testData[i], testData[6], testData[7], testData[8], testData[9]);
				actualBoolArray.add(isElementPresent(studObj.firstNameValidationMsg));
				if(studObj.createStudent.isEnabled())
					actualBoolArray.add(true);
				else 
					actualBoolArray.add(false);
			}			
		}catch (Exception e) {
			System.out.println("verifyLengthValidation Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}
		System.out.println("verifyLengthValidation\nActual: "+actualBoolArray+"\nExpected: "+expectedBoolArray);
		Assert.assertEquals(actualBoolArray,expectedBoolArray);
	}
	
	@Test(priority = 6)
	public void verifyEmailValidation() {
		try {			
			log.info("Entered verifyEmailValidation method");
			actualBoolArray = new ArrayList<>();
			expectedBoolArray = new ArrayList<>();
			eTest = eReports.createTest("verifyEmailValidation");
			eTest.assignCategory("StudentList");
			expectedBoolArray.add(true);expectedBoolArray.add(false);//warning message present & submit button inactive
			expectedBoolArray.add(true);expectedBoolArray.add(false);//warning message not present & submit button inactive
			expectedBoolArray.add(true);expectedBoolArray.add(false);//warning message not present & submit button  inactive
			expectedBoolArray.add(true);expectedBoolArray.add(false);//warning message not present & submit button  inactive
			expectedBoolArray.add(false);expectedBoolArray.add(true);//warning message not present & submit button active
			expectedBoolArray.add(false);expectedBoolArray.add(true);//warning message not present & submit button active			
			
			for(int i=0;i<6;i++) {
				testData = studDataObj.getEmailValidationData();			
				waitForElementToLoad(dashboardObj.studentList);
				dashboardObj.studentList.click();				
				waitForElementToLoad(studObj.addNewStudent);
				studObj.addNewStudent.click();				
				waitForElementToLoad(studObj.back);
				waitForElementToLoad(studObj.firstName);
				studObj.mandatoryFieldsValidation(testData[6], testData[i], testData[7], testData[8], testData[9]);				
				actualBoolArray.add(isElementPresent(studObj.emailValidationMsg));
				
				if(studObj.createStudent.isEnabled())
					actualBoolArray.add(true);
				else 
					actualBoolArray.add(false);
			}			
		}
		catch(Exception e){
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
			Assert.assertEquals(actualBoolArray,expectedBoolArray);
		}
		System.out.println("verifyEmailValidation\nActual: "+actualBoolArray+"\nExpected: "+expectedBoolArray);
		Assert.assertEquals(actualBoolArray,expectedBoolArray);
	}	
	
	@BeforeClass
	@Parameters("fakerData")
	public void initialize() {
		try {
			log.info("Entered initialize method");
			sigInObj = new SignInObject(driver);
			studObj = new StudentListObject(driver);
			dashboardObj = new DashboardObject(driver);			
			studDataObj = new StudentListData();			
			
			
			
			/*
			 * signInObjAnd = new SignInObjectAndroid(androidDriver); onboardingObjAnd = new
			 * OnboradingObjectAndroid(androidDriver); homeObjAnd = new
			 * HomeScreenObjectAndroid(androidDriver); profileObjAnd = new
			 * ProfileObjectAndroid(androidDriver);
			 */
			 
			 
			
			tdImport = new TestDataImport();
			tdImport.readExcel("StudentList");
			if(!suiteName.contains("Failed suite"))
				studDataObj.generateFakeData();
			waitForElementToLoad(sigInObj.email);
			sigInObj.signIn("automation@mailinator.com",  "Strongpassword1");
			waitForElementToLoad(dashboardObj.users);			
			dashboardObj.users.click();
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
	
	@AfterClass
	public void logout() {
		try {
			log.info("Sign In: logout");
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			waitIfElementClickIsIntercepted(dashboardObj.logout, "click", "");
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
}
