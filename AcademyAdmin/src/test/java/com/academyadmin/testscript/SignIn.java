package com.academyadmin.testscript;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.academyadmin.baseclass.SetUp;
import com.academyadmin.objectrepository.DashboardObject;
import com.academyadmin.objectrepository.SignInObject;
import com.academyadmin.testdata.SignInData;
import com.academyadmin.testdata.TestDataImport;
import com.aventstack.extentreports.Status;

public class SignIn extends SetUp {
	SignInObject sigInObj;
	DashboardObject dashboardObj;
	SignInData signInDataObj;
	TestDataImport tdImport;
	String[] testData;
	
	@Test(priority = 0)
	public void invalidEmailInvaildPassword() {
		try {
			log.info("Sign In: invalidEmailInvaildPassword");
			actualString=""; expectedString="";
			eTest = eReports.createTest("invalidEmailInvaildPassword");
			eTest.assignCategory("SignIn");
			expectedString = "Invalid credentials";	
			testData = signInDataObj.getSignInInvalidData();
			
			sigInObj.signIn(testData[0], testData[1]);						
			actualString = staleExceptionOnMessage(sigInObj.invalidCredentials);		
		}catch (Exception e) {			
			System.out.println("invalidEmailInvaildPassword Failed\n");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("Actual: "+actualString+"\nExpcted: "+expectedString);
		Assert.assertEquals(actualString , expectedString);		
	}
	
	@Test(priority = 1)
	public void validEmailValidPassword() {
		try {
			log.info("Sign In: validEmailValidPassword");
			actualString=""; expectedString="";
			eTest = eReports.createTest("validEmailValidPassword");
			eTest.assignCategory("SignIn");
			expectedString = "Dashboard";
			testData = signInDataObj.getSignInValidData();
			
			waitForElementToLoad(sigInObj.email);
			sigInObj.signIn(testData[0],  testData[1]);			
			waitForElementToLoad(dashboardObj.dashboard);
			actualString = dashboardObj.dashboard.getText();
			
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			dashboardObj.logout.click();
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("validEmailValidPassword Failed");
			log.info(e);
			eTest.log(Status.FAIL,"Exception: "+ e);
		}		
		System.out.println("Actual: "+actualString+"\nExpcted: "+expectedString);
		Assert.assertEquals(actualString , expectedString);
	}
	
	@BeforeClass
	public void initilaize() {
		try {
			log.info("Sign In: initilaize");
			sigInObj = new SignInObject(driver);
			dashboardObj = new DashboardObject(driver);
			signInDataObj = new SignInData();
			tdImport = new TestDataImport();
			tdImport.readExcel("SignIn");
			waitForElementToLoad(sigInObj.email);
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
}
