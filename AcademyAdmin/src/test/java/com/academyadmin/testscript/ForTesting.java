package com.academyadmin.testscript;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ForTesting {
	WebDriver driver;
	public String projectFolder = System.getProperty("user.dir");
	public String chromeDriverPath = projectFolder+"/Drivers/chromedriver.exe";
	public String firefoxDriverPath = projectFolder+"/Drivers/geckodriver.exe";
	
	@Test
	public void tester2() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.id("email")).sendKeys("superadmin02@company.com");
		driver.findElement(By.id("password")).sendKeys("Strongpassword1");
		driver.findElement(By.id("login-form-submit")).click();
	}
	
	
	
	  @BeforeTest
	  
	  @Parameters("browser") 
	  public void precodition(String browser) 
	  {
	  if(browser.equals("chrome")) { 
		  System.setProperty("webdriver.chrome.driver",	  chromeDriverPath); driver = new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.navigate().to("https://admin-uat.darisni.me/auth/login"); 
	  } else
	  if(browser.equals("firefox")) { 
	  System.setProperty("webdriver.gecko.driver",	  firefoxDriverPath); driver= new FirefoxDriver();
	  driver.manage().window().maximize();
	  driver.navigate().to("https://admin-uat.darisni.me/auth/login"); 
	 } 
	  }
	 
}
