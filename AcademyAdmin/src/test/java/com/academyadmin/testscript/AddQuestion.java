package com.academyadmin.testscript;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.academyadmin.baseclass.SetUp;
import com.academyadmin.objectrepository.AcademyDashboardObject;
import com.academyadmin.objectrepository.AddQuestionObject;
import com.academyadmin.objectrepository.DashboardObject;
import com.academyadmin.objectrepository.QuestionListObject;
import com.academyadmin.objectrepository.QuestionViewObject;
import com.academyadmin.objectrepository.SignInObject;
import com.academyadmin.testdata.AddQuestionData;
import com.academyadmin.testdata.TestDataImport;
import com.aventstack.extentreports.Status;

public class AddQuestion extends SetUp {
	SignInObject sigInObj;
	AddQuestionObject addQstnobj;
	AddQuestionData addQstnDataObj;
	QuestionListObject quesListObj;
	QuestionViewObject quesViewObj;
	DashboardObject dashboardObj;
	AcademyDashboardObject acadmeyDashboardObj;
	TestDataImport tdImport;
	
	String split = "";
	String[] testData;
	String[] optionSplit;
			
	@Test(priority = 0)
	public void verifyAddQuestion() {
		try {
			log.info("AddQuestion: verifyAddQuestion");
			actualArray = new ArrayList<>();
			expectedArray = new ArrayList<>();
			eTest = eReports.createTest("verifyAddQuestion");
			eTest.assignCategory("Add Question");
			expectedArray.add("Question has been created");
			testData = addQstnDataObj.getAddQstnData();

			for (int i = 0; i < 17; i++) {
				if (i != 7 && i != 8 && i != 9 && i != 10 && i != 11 && i != 13 && i != 14)
					expectedArray.add(testData[i]);				
				else if (i == 7 && testData[7].equals("true"))
					expectedArray.add("Exam");
				else if (i == 8 && testData[8].equals("true"))
					expectedArray.add(" Test");
				else if (i == 9 && testData[9].equals("true"))
					expectedArray.add(" Quiz");				
				else if (i == 10 && testData[10].equals("true"))
					expectedArray.add(" Exercise");				
				else if (i == 11 && testData[11].equals("true"))
					expectedArray.add(" Past-exam");
				else if (i == 13) {
					optionSplit = testData[i].split(",");
					for(int j=0;j<optionSplit.length;j++) {
						expectedArray.add(optionSplit[j]);
					}
				}
			}
			expectedArray.set(5, testData[4] + "ed");

			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addQuestion, "click", "");
			waitForElementToLoad(addQstnobj.language);
			addQstnobj.questionAdd(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5],
					testData[6], Boolean.parseBoolean(testData[7]), Boolean.parseBoolean(testData[8]),
					Boolean.parseBoolean(testData[9]), Boolean.parseBoolean(testData[10]),
					Boolean.parseBoolean(testData[11]), testData[12], testData[13], testData[14], testData[15],
					testData[16]);

			waitForElementToLoad(addQstnobj.questionCreateMsg);
			actualArray.add(addQstnobj.questionCreateMsg.getText());
			
			waitForElementToLoad(quesListObj.selectSubject);
			quesListObj.questionSearch(testData[1], testData[2], testData[3], testData[4],testData[12]);
			waitIfElementClickIsIntercepted(quesListObj.view, "click", "");
			waitForElementToLoad(quesViewObj.language);
			
			actualArray.add(quesViewObj.language.getText());
			actualArray.add(quesViewObj.subject.getText());
			actualArray.add(quesViewObj.topic.getText());
			actualArray.add(quesViewObj.type.getText());
			actualArray.add(quesViewObj.status.getText());
			actualArray.add(quesViewObj.displayProbability.getText());
			actualArray.add(quesViewObj.questionNumber.getText());

			String[] namesList = quesViewObj.appearedIn.getText().split(",");
			
			for(int i=0;i<namesList.length;i++) {
				actualArray.add(namesList[i]);
			}
			
			actualArray.add(quesViewObj.question.getText());
			
			for(int i=0;i<optionSplit.length;i++) {				
				String name = driver.findElement(By.xpath("//label[contains(text(),'"+optionSplit[i]+"')]")).getText();
				actualArray.add(name.substring(name.lastIndexOf(".") + 1).trim());
			}
			String answer = quesViewObj.correctAnswer.getText();
			actualArray.add("Option "+answer.substring(0,answer.indexOf(".")));
			actualArray.add(quesViewObj.answerDesciption.getText());
		} catch (Exception e) {
			System.out.println("verifyAddQuestion Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL, "Exception: " + e);
		}
		System.out.println("verifyAddQuestion\nActual: " + actualArray + "\nExpcted: " + expectedArray);
		Assert.assertEquals(actualArray, expectedArray);
	}
	
	//@Test(priority = 1)
	public void verifyEditQuestions() {
		try {
			log.info("AddQuestion: verifyEditQuestion");
			actualArray = new ArrayList<>();
			expectedArray = new ArrayList<>();
			eTest = eReports.createTest("verifyEditQuestion");
			eTest.assignCategory("Add Question");
			expectedArray.add("Question details have been updated");
			addQstnDataObj.getAddQstnData();

			for (int i = 0; i < 19; i++) {
				if (i != 7 && i != 8 && i != 9 && i != 10 && i != 11 && i != 14 && i != 16 && i != 17)
					expectedArray.add(testData[i]);
			}
			expectedArray.set(5, testData[4] + "ed");

			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addQuestion, "click", "");
			waitForElementToLoad(addQstnobj.language);
			addQstnobj.questionAdd(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5],
					testData[6], Boolean.parseBoolean(testData[7]), Boolean.parseBoolean(testData[8]),
					Boolean.parseBoolean(testData[9]), Boolean.parseBoolean(testData[10]),
					Boolean.parseBoolean(testData[11]), testData[12], testData[13], testData[14], testData[15],
					testData[16]);
 		   
			waitForElementToLoad(quesListObj.selectSubject);
			quesListObj.questionSearch(testData[1], testData[2], testData[3], testData[4],testData[12]);
			waitIfElementClickIsIntercepted(quesListObj.edit, "click", "");
			
			addQstnDataObj.getEditQstnData();
			waitForElementToLoad(addQstnobj.type);
			addQstnobj.questionEdit(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5],
					testData[6],testData[7],testData[8]);
			waitForElementToLoad(quesListObj.questionEditMsg);
			actualArray.add(quesListObj.questionEditMsg.getText());
			
			waitForElementToLoad(quesListObj.selectSubject);
			quesListObj.questionSearch(testData[1], testData[2], testData[3], testData[4],testData[4]);
			waitIfElementClickIsIntercepted(quesListObj.view, "click", "");
			waitForElementToLoad(addQstnobj.questionNumber);
			
			actualArray.add(quesViewObj.language.getText());
			actualArray.add(quesViewObj.subject.getText());
			actualArray.add(quesViewObj.topic.getText());
			actualArray.add(quesViewObj.type.getText());
			actualArray.add(quesViewObj.status.getText());
			actualArray.add(quesViewObj.displayProbability.getText());
			actualArray.add(quesViewObj.questionNumber.getText());

			String[] namesList = quesViewObj.appearedIn.getText().split(",");
			
			for(int i=0;i<namesList.length;i++) {
				actualArray.add(namesList[i]);
			}
			
			actualArray.add(quesViewObj.question.getText());
			/*
			 * split = quesViewObj.option1.getText();
			 * actualArray.add(split.substring(split.lastIndexOf(".") + 1).trim()); split =
			 * quesViewObj.option2.getText();
			 */
			actualArray.add(split.substring(split.lastIndexOf(".") + 1).trim());
			actualArray.add(quesViewObj.answerDesciption.getText());
			
		} catch (Exception e) {
			System.out.println("verifyEditQuestion Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL, "Exception: " + e);
		}
		System.out.println("verifyEditQuestion\nActual: " + actualArray + "\nExpcted: " + expectedArray);
		Assert.assertEquals(actualArray, expectedArray);
	}
	
	@Test(priority = 2)
	public void verifyDeleteQuestion() {
		try {
			log.info("AddQuestion: verifyDeleteQuestion");
			actualArray = new ArrayList<>();
			expectedArray = new ArrayList<>();
			eTest = eReports.createTest("verifyDeleteQuestion");
			eTest.assignCategory("Add Question");
			expectedArray.add("Question has been deleted");
			expectedArray.add("No records found");
			testData = addQstnDataObj.getDeleteQstnData();

			waitIfElementClickIsIntercepted(acadmeyDashboardObj.burgerMenu, "click", "");
			waitIfElementClickIsIntercepted(acadmeyDashboardObj.addQuestion, "click", "");
			waitForElementToLoad(addQstnobj.language);
			
			addQstnobj.questionAdd(testData[0], testData[1], testData[2], testData[3], testData[4], testData[5],
					testData[6], Boolean.parseBoolean(testData[7]), Boolean.parseBoolean(testData[8]),
					Boolean.parseBoolean(testData[9]), Boolean.parseBoolean(testData[10]),
					Boolean.parseBoolean(testData[11]), testData[12], testData[13], testData[14], testData[15],
					testData[16]);

			waitForElementToLoad(quesListObj.selectSubject);
			quesListObj.questionSearch(testData[1], testData[2], testData[3], testData[4],testData[12]);
			waitIfElementClickIsIntercepted(quesListObj.delete, "click", "");
			waitIfElementClickIsIntercepted(quesListObj.deleteYes, "click", "");
			
			waitForElementToLoad(quesListObj.questiondltMsg);
			actualArray.add(quesListObj.questiondltMsg.getText());			
			
			quesListObj.clear.click();
			waitForElementToLoad(quesListObj.selectSubject);
			quesListObj.questionSearch(testData[1], testData[2], testData[3], testData[4], testData[12]);
			
			waitForElementToLoad(quesListObj.noRecordsFound);
			actualArray.add(quesListObj.noRecordsFound.getText());
		}
		catch(Exception e) {
			System.out.println("verifyEditQuestion Failed");
			e.printStackTrace();
			log.info(e);
			eTest.log(Status.FAIL, "Exception: " + e);
		}
		System.out.println("verifyDeleteQuestion\nActual: " + actualArray + "\nExpcted: " + expectedArray);
		Assert.assertEquals(actualArray, expectedArray);
	}

	@BeforeClass
	public void initilaize() {
		try {
			log.info("AddQuestion: initilaize");
			sigInObj = new SignInObject(driver);
			addQstnobj = new AddQuestionObject(driver);
			quesListObj = new QuestionListObject(driver);
			quesViewObj = new QuestionViewObject(driver);
			addQstnDataObj = new AddQuestionData();
			dashboardObj = new DashboardObject(driver);
			acadmeyDashboardObj = new AcademyDashboardObject(driver);
			
			tdImport = new TestDataImport();
			tdImport.readExcel("AddQuestion");
			if(!suiteName.contains("Failed suite"))
				addQstnDataObj.generateFakeData();
			
			waitForElementToLoad(sigInObj.email);
			sigInObj.signIn("automation@mailinator.com",  "Strongpassword1");
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			dashboardObj.academyAadmin.click();	
			chromeTab = new ArrayList<>(driver.getWindowHandles());
			driver.switchTo().window(chromeTab.get(1));
		} catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}
	}
	
	@AfterClass
	public void logout() {
		try {
			log.info("AddQuestion: logout");
			driver.switchTo().window(chromeTab.get(0));
			waitForElementToLoad(dashboardObj.experionAdmin);
			dashboardObj.experionAdmin.click();
			waitIfElementClickIsIntercepted(dashboardObj.logout, "click", "");
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
}
