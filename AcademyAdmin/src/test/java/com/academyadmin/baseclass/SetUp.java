package com.academyadmin.baseclass;

import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.academyadmin.testdata.DeviceDetails;
import com.academyadmin.testdata.TestDataImport;
import com.academyadmin.utility.Utilities;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class SetUp {	
	public static WebDriver driver;
	public static AppiumDriver<MobileElement> androidDriver;
	public WebDriverWait wait30Sec, wait600Sec;
	public ChromeOptions options;
	public DesiredCapabilities caps;
	public Capabilities cap;
	@SuppressWarnings("rawtypes")
	public TouchAction action;
	public static Utilities utilityObj;
	public TestDataImport tdImport;
	public DeviceDetails dd;
	public String[] deviceDetails;
	public DateFormat df;
	public Date date;
	public File directory, apk;
	public String projectFolder = System.getProperty("user.dir");
	public String excelPath = projectFolder+"/Excel/Testdata.xls";
	public String chromeDriverPath = projectFolder+"/Drivers/chromedriver.exe";
	public String firefoxDriverPath = projectFolder+"/Drivers/geckodriver.exe";
	public String edgeDriverPath = projectFolder+"/Drivers/msedgedriver.exe";
	public String extendReportPath = projectFolder+"/Reports/";
	public String log4jConfPath = projectFolder+"/src/test/java/com/academyadmin/resources/Log4j.properties";
	public String apkpath = projectFolder+"/Darisni-v435-dars_staging-release.apk";	
	public String logPath = "";
	public static String suiteName ="";
	public static String reportTimestamp="";
	public static String reportDir="";
	public static ExtentSparkReporter esReporter;
	public static ExtentReports eReports;
	public static ExtentTest eTest;
	public static Logger log;
	public static String actualString = "",  expectedString = "", sheetName = "", screenshotName = "", currentDateTime = "";
	public static String email="", password="";
	public static Boolean actualBool, expectedBool;
	public static ArrayList<String> testData1 = new ArrayList<String>();	
	public static ArrayList<Boolean> actualBoolArray = new ArrayList<Boolean>();
	public static ArrayList<Boolean> expectedBoolArray = new ArrayList<Boolean>();	
	public static ArrayList<String> actualArray = new ArrayList<String>();
	public static ArrayList<String> expectedArray = new ArrayList<String>();
	public static ArrayList<String> chromeTab = new ArrayList<String>();	
	
	@BeforeSuite
	@Parameters("browser")
	public void precondition(ITestContext context,@Optional("chrome") String browser) throws InterruptedException {
		try {				
			log = Logger.getLogger(SetUp.class.getName());
			//mobileDriver();
			reportTimestamp = getCurrentDateTime();//for providing current data and time for log file, use the same time stamp in report zip for relative path
			reportDir = "C:/Darisni_Automation/Reports/Report_"+reportTimestamp;//creates report folder with time stamp
			System.setProperty("logPath", "C:/Darisni_Automation/Logs/LogFile_");
			PropertyConfigurator.configure(log4jConfPath);//Configured log files path	
			utilityObj = new Utilities();
			utilityObj.createDirectoryIfNotExist();//for log, report, screenshot
			if(browser.equals("chrome"))
				chromeDriver();
			else if(browser.equals("firefox"))
				firefoxDriver();
			//edgeDriver();
	        extendReport();//initializing report
	        suiteName = context.getCurrentXmlTest().getSuite().getName();
	        driver.manage().window().maximize();
			driver.navigate().to("https://admin-uat.darisni.me/auth/login");
			log.info("Page loaded");
		}catch (Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}	
	
	@AfterMethod
	public void testReportResult(ITestResult result) {
		try {
			log.info("Report status initiated");			
			//Passed test status along with screenshot will captured here
			if(result.getStatus() == ITestResult.SUCCESS) {
				eTest .log(Status.PASS, "Test passed");
			}
			
			//Failed test status along with screenshot will captured here
			else if(result.getStatus() == ITestResult.FAILURE) {
				eTest .log(Status.FAIL, "Test failed");
			}
			
			//Skipped test status along with screenshot will captured here
			else if(result.getStatus() == ITestResult.SKIP) {
				eTest.log(Status.SKIP, "Test failed");
			}
			
			screenshotName = utilityObj.getScreenshot(driver, result.getName()+"_"+getCurrentDateTime());
			eTest.addScreenCaptureFromPath(screenshotName);
		}catch(Exception e) {
			e.printStackTrace();
			log.info(e);
		}		
	}
	
	@AfterSuite
	public void endSuite() {
		try {			
			eReports.flush();
			utilityObj.createZip(reportDir);
			driver.quit();
		}catch(Exception e) {
			e.printStackTrace();
			log.info(e);
		}
	}
	
	public void chromeDriver() {
		try {
			System.setProperty("webdriver.chrome.driver", chromeDriverPath);
			options = new ChromeOptions();
			
			//for disabling "Chrome is being controlled by automated test software" message
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));
			
			//for disabling username/password saving option
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);			
			options.setExperimentalOption("prefs", prefs);
			
	        //options.addArguments("--headless");//for browser less script running
	        driver = new ChromeDriver(options);
	        cap = ((RemoteWebDriver) driver).getCapabilities();//for retrieving browser name which is printed in reports
		}catch(Exception e) {
			e.printStackTrace();
			log.info(e);
		}	
	}	

	@SuppressWarnings("deprecation")
	public void firefoxDriver() {
		try {
			System.setProperty("webdriver.gecko.driver", firefoxDriverPath);
			cap = DesiredCapabilities.firefox();
			((MutableCapabilities) cap).setCapability("marionette",true);
			driver= new FirefoxDriver(cap);
	        cap = ((RemoteWebDriver) driver).getCapabilities();//for retrieving browser name which is printed in reports
		}catch(Exception e) {
			e.printStackTrace();
			log.info(e);
		}
	}
	
	public void edgeDriver() {
		try {
			System.setProperty("webdriver.edge.driver", edgeDriverPath);
			driver= new EdgeDriver();
		}catch(Exception e) {
			e.printStackTrace();
			log.info(e);
		}
	}
	
	public void mobileDriver() {
		for(int i=0;i<5;i++) {
			try {
				tdImport = new TestDataImport();
				dd = new DeviceDetails();
				tdImport.readExcel("DeviceDetails");
				deviceDetails = dd.getDeviceData();
				caps = new DesiredCapabilities();
				caps.setCapability(MobileCapabilityType.PLATFORM_NAME, deviceDetails[0]);
				//caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0.0");
				caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceDetails[1]);
				//caps.setCapability(MobileCapabilityType.DEVICE_NAME, "OnePlus 3T");
				caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceDetails[2]);
				//caps.setCapability(MobileCapabilityType.UDID, "e98da357");
				caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "900");
		        //caps.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY,"com.darisni.app.activity.other.LandingPageActivity");
				apk = new File(deviceDetails[3]);
				caps.setCapability(MobileCapabilityType.APP, apk.getAbsolutePath());
				caps.setCapability(MobileCapabilityType.NO_RESET, false);
				androidDriver=new AppiumDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
				break;
			}catch(SessionNotCreatedException e) {
				System.out.println("Error occurred while creating session, retrying...");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void extendReport() {
		try {
			log.info("Report initiated");
			esReporter = new ExtentSparkReporter(reportDir + "/TestReport_"+getCurrentDateTime()+".html");
			esReporter.config().setDocumentTitle("Automation Report");
			esReporter.config().setReportName("Darisni Admin Automation Report");
			esReporter.config().setTheme(Theme.STANDARD);
			eReports = new ExtentReports();
			eReports.attachReporter(esReporter);
			eReports.setSystemInfo("Project Name", "Darisni Academy");
			eReports.setSystemInfo("Platform", System.getProperty("os.name"));
			eReports.setSystemInfo("Environment", "QA");
			eReports.setSystemInfo("Browser", cap.getBrowserName().substring(0,1).toUpperCase() + cap.getBrowserName().substring(1).toLowerCase());
		}catch(Exception e) {
			e.printStackTrace();
			log.info(e);
		}
	}	
	
	@SuppressWarnings("rawtypes")
	public void scrollUp(WebElement element) {		
		for(int i=0,j=300,k=1000;i<5;i++) {
			try {
				action = new TouchAction(androidDriver);
				action.press(PointOption.point(0,k)).
				waitAction(new WaitOptions().withDuration(Duration.ofMillis(3000))) //you can change wait durations as per your requirement
				.moveTo(PointOption.point(0, j))
				.release()
				.perform();
				wait30Sec = new WebDriverWait(driver, 05);
				wait30Sec.until(ExpectedConditions.visibilityOf(element));
				element.isDisplayed();
				break;
			}catch(Exception e) {
				j+=300;
				k+=300;
			}
		}	
	}
		
	public boolean isElementPresent(WebElement element) {
		try {
			element.isDisplayed();
			return true;
		}catch(Exception e) {			
			return false;			
		}	
	}
	public boolean isElementEnabled(WebElement element) {
		try {
			if(element.getAttribute("disabled").equals(null))
			return true;
			else 
				return false;
		}catch(Exception e) {			
			return true;			
		}	
	}
	public boolean  isChecked(WebElement element) {
		try {
		 element.isSelected();
			return true;
			
		}catch(Exception e) {			
			return false;			
		}	
	}
	
	
	//handling gamification popus
	public void handlePopups(WebElement[] element) {
		wait30Sec = new WebDriverWait(driver, 02);
		for(int i=0;i<element.length;i++) {
			try {			
				wait30Sec.until(ExpectedConditions.visibilityOf(element[i]));
				element[i].click();
			}catch (Exception e) {
				
			}
		}		
	}
	
	
	public void clickIfElementIsPresent(WebElement element) {
		try {
			wait30Sec = new WebDriverWait(driver, 05);
			wait30Sec.until(ExpectedConditions.visibilityOf(element));
			element.click();
		}catch(Exception e) {
			System.out.println("Waited for 5 seconds");
		}		
	}
	
	public void waitForElementToLoad(WebElement element) {
		try {
			wait30Sec = new WebDriverWait(driver, 30);//for normal explicit wait
			wait30Sec.until(ExpectedConditions.visibilityOf(element));
		}catch (Exception e) {
			System.out.println("Waited for 30 seconds");
			e.printStackTrace();
		}		
	}
	
	public void fileUploadWait(WebElement element) {
		try {
			wait600Sec = new WebDriverWait(driver, 600);//for normal explicit wait
			wait600Sec.until(ExpectedConditions.visibilityOf(element));		
		}catch (Exception e) {
			System.out.println("Waited for 600 seconds");
			e.printStackTrace();
		}		
	}
	
	public void waitIfElementClickIsIntercepted(WebElement element, String action, String value) {
		wait30Sec = new WebDriverWait(driver, 30);//for normal explicit wait
		for(int i=0;i<20;i++) {
			try {
				wait30Sec.until(ExpectedConditions.visibilityOf(element));
				if(action.equals("click")||action.equals("checkbox"))
					element.click();
				else if(action.equals("select"))
					element.sendKeys(value);
				break;
			}catch(ElementClickInterceptedException e) {
				System.out.println("Click Intercepted: waitIfElementClickIsIntercepted");
			}catch(StaleElementReferenceException e) {
				System.out.println("Stale Exception: waitIfElementClickIsIntercepted");
			}
		}				
	}
	
	public String staleExceptionOnMessage(WebElement element) {
		String message="";
		wait30Sec = new WebDriverWait(driver, 02);//for normal explicit wait
		for(int i=0;i<5;i++) {
			try {
				wait30Sec.until(ExpectedConditions.visibilityOf(element));
				message = element.getText();
				break;
			}catch(StaleElementReferenceException e) {
				System.out.println("Stale Exception: staleExceptionOnMessage");
			}catch(NoSuchElementException e) {	
				System.out.println("No Such Element: staleExceptionOnMessage");
			}catch (Exception e) {
				System.out.println("Exception: staleExceptionOnMessage");
			}
		}
		return message;
	}
	
	public String getCurrentDateTime() {
		try {
			log.info("Current date and time");
			df= new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
			date = new Date();
			System.setProperty("currentDateTime", df.format(new Date()));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return currentDateTime = df.format(date);
	}
}
