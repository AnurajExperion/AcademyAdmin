package com.academyadmin.testdata;

import java.util.Locale;

import com.academyadmin.baseclass.SetUp;
import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class ScormCourseData extends SetUp {
	Locale local = new Locale("en-IND");
	Faker fake = new Faker(local);
	FakeValuesService fakeService = new FakeValuesService(local, new RandomService());
	TestDataImport tdImport = new TestDataImport();
	String[] testData;
	
	//generate faker data for unique fields and write it to excel
	public void generateFakeData() {
		tdImport.setValue(1, 2, fake.name().title());
		tdImport.setValue(2, 2, fake.name().title());
		tdImport.setValue(3, 2, fake.name().title());
		tdImport.setValue(4, 2, fake.name().title());
		tdImport.setValue(5, 2, fake.name().title());
		tdImport.setValue(6, 2, fake.name().title());
		tdImport.setValue(7, 2, fake.name().title());
		tdImport.setValue(8, 2, fake.name().title());
		tdImport.setValue(9, 2, fake.name().title());
	}
	
	public String[] getAddScormData() {
		testData = new String[11];
		testData[0] = tdImport.getValue(1, 1);
		testData[1] = tdImport.getValue(1, 2);
		testData[2] = tdImport.getValue(1, 3);
		testData[3] = tdImport.getValue(1, 4);
		testData[4] = tdImport.getValue(1, 5);
		testData[5] = tdImport.getValue(1, 6);
		testData[6] = tdImport.getValue(1, 7);
		testData[7] = tdImport.getValue(1, 9);
		testData[8] = tdImport.getValue(1, 10);
		testData[9] = tdImport.getValue(1, 11);
		testData[10] = tdImport.getValue(1, 12);
		return testData;
	}
	
	public String[] getValidScormData() {
		testData = new String[11];
		testData[0] = tdImport.getValue(2, 1);
		testData[1] = tdImport.getValue(2, 2);
		testData[2] = tdImport.getValue(2, 3);
		testData[3] = tdImport.getValue(2, 4);
		testData[4] = tdImport.getValue(2, 5);
		testData[5] = tdImport.getValue(2, 6);
		testData[6] = tdImport.getValue(2, 7);
		testData[7] = tdImport.getValue(2, 9);
		testData[8] = tdImport.getValue(2, 10);
		testData[9] = tdImport.getValue(2, 11);
		testData[10] = tdImport.getValue(2, 12);
		return testData;
	}
	
	public String[] getInvalidScormData() {
		testData = new String[11];
		testData[0] = tdImport.getValue(3, 1);
		testData[1] = tdImport.getValue(3, 2);
		testData[2] = tdImport.getValue(3, 3);
		testData[3] = tdImport.getValue(3, 4);
		testData[4] = tdImport.getValue(3, 5);
		testData[5] = tdImport.getValue(3, 6);
		testData[6] = tdImport.getValue(3, 7);
		testData[7] = tdImport.getValue(3, 9);
		testData[8] = tdImport.getValue(3, 10);
		testData[9] = tdImport.getValue(3, 11);
		testData[10] = tdImport.getValue(3, 12);
		return testData;
	}
	
	public String[] getFieldStatusWhileEditData() {
		testData = new String[11];
		testData[0] = tdImport.getValue(4, 1);
		testData[1] = tdImport.getValue(4, 2);
		testData[2] = tdImport.getValue(4, 3);
		testData[3] = tdImport.getValue(4, 4);
		testData[4] = tdImport.getValue(4, 5);
		testData[5] = tdImport.getValue(4, 6);
		testData[6] = tdImport.getValue(4, 7);
		testData[7] = tdImport.getValue(4, 9);
		testData[8] = tdImport.getValue(4, 10);
		testData[9] = tdImport.getValue(4, 11);
		testData[10] = tdImport.getValue(4, 12);
		return testData;
	}
	
	public String[] getEditBeforeSuccessData() {
		testData = new String[20];
		testData[0] = tdImport.getValue(5, 1);
		testData[1] = tdImport.getValue(5, 2);
		testData[2] = tdImport.getValue(5, 3);
		testData[3] = tdImport.getValue(5, 4);
		testData[4] = tdImport.getValue(5, 5);
		testData[5] = tdImport.getValue(5, 6);
		testData[6] = tdImport.getValue(5, 7);
		testData[7] = tdImport.getValue(5, 9);
		testData[8] = tdImport.getValue(5, 10);
		testData[9] = tdImport.getValue(5, 11);
		testData[10] = tdImport.getValue(5, 12);
		testData[11] = tdImport.getValue(6, 1);
		testData[12] = tdImport.getValue(6, 2);
		testData[13] = tdImport.getValue(6, 3);
		testData[14] = tdImport.getValue(6, 4);
		testData[15] = tdImport.getValue(6, 5);
		testData[16] = tdImport.getValue(6, 6);
		testData[17] = tdImport.getValue(6, 7);
		testData[18] = tdImport.getValue(6, 9);
		testData[19] = tdImport.getValue(6, 9);
		return testData;
	}
	
	public String[] getEditAfterSuccessData() {
		testData = new String[14];
		testData[0] = tdImport.getValue(7, 1);
		testData[1] = tdImport.getValue(7, 2);
		testData[2] = tdImport.getValue(7, 3);
		testData[3] = tdImport.getValue(7, 4);
		testData[4] = tdImport.getValue(7, 5);
		testData[5] = tdImport.getValue(7, 6);
		testData[6] = tdImport.getValue(7, 7);
		testData[7] = tdImport.getValue(7, 9);
		testData[8] = tdImport.getValue(7, 10);
		testData[9] = tdImport.getValue(7, 11);
		testData[10] = tdImport.getValue(7, 12);
		testData[11] = tdImport.getValue(8, 12);
		testData[12] = tdImport.getValue(8, 12);
		testData[13] = tdImport.getValue(8, 12);
		return testData;
	}
	
	public String[] getScormDeleteData() {
		testData = new String[11];
		testData[0] = tdImport.getValue(9, 1);
		testData[1] = tdImport.getValue(9, 2);
		testData[2] = tdImport.getValue(9, 3);
		testData[3] = tdImport.getValue(9, 4);
		testData[4] = tdImport.getValue(9, 5);
		testData[5] = tdImport.getValue(9, 6);
		testData[6] = tdImport.getValue(9, 7);
		testData[7] = tdImport.getValue(9, 9);
		testData[8] = tdImport.getValue(9, 10);
		testData[9] = tdImport.getValue(9, 11);
		testData[10] = tdImport.getValue(9, 12);
		return testData;
	}
	
	public String[] getTitleMinLengthValidationData() {
		testData = new String[4];
		testData[0] = tdImport.getValue(10, 2);
		testData[1] = tdImport.getValue(11, 2);
		testData[2] = tdImport.getValue(12, 2);
		testData[3] = tdImport.getValue(13, 2);
		return testData;
	}
}
