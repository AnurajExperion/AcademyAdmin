package com.academyadmin.testdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import com.academyadmin.baseclass.SetUp;

public class TestDataImport extends SetUp {
	static HSSFWorkbook wb;
	static HSSFSheet sh;
	static Cell cell;
	FileInputStream inFile;
	FileOutputStream outFile;
	String cellData = "";

	public void readExcel(String sheetName) {
		try {
			log.info("Entered readExcel method");
			inFile = new FileInputStream(excelPath);
			wb = new HSSFWorkbook(inFile);
			sh= wb.getSheet(sheetName);			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("readExcel Failed");
			log.info(e);
		}
	}
	
	public String getValue(int i, int j) {
		try {
			log.info("Entered getValue method");
			cellData = sh.getRow(i).getCell(j).toString();
		}catch(Exception e){
			cellData = "";
			log.info(e);
		}
		return cellData;
	}
	
	public void setValue(int row, int col, String value) {
		try {
			outFile =new FileOutputStream(new File(excelPath));
			cell = sh.getRow(row).getCell(col);
			cell.setCellValue(value);          
            wb.write(outFile);
            outFile.close();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Failed");
		}
	}
}
