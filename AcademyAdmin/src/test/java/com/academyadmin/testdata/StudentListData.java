package com.academyadmin.testdata;

import java.util.Locale;

import com.academyadmin.baseclass.SetUp;
import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class StudentListData extends SetUp{
	Locale local = new Locale("en-IND");
	Faker fake = new Faker(local);
	FakeValuesService fakeService = new FakeValuesService(local, new RandomService());	
	TestDataImport tdImport = new TestDataImport();
	String[] testData;
	
	//generate faker data for unique fields and write it to excel
	public void generateFakeData() {
		tdImport.setValue(1, 1, fake.name().firstName());
		tdImport.setValue(2, 1, fake.name().firstName());
		tdImport.setValue(3, 1, fake.name().firstName());
		
		tdImport.setValue(1, 2, fake.name().lastName());
		tdImport.setValue(2, 2, fake.name().lastName());
		tdImport.setValue(3, 2, fake.name().lastName());
		
		tdImport.setValue(1, 3, fakeService.bothify("????####@mailinator.com"));
		tdImport.setValue(2, 3, fakeService.bothify("????####@mailinator.com"));
		tdImport.setValue(3, 3, fakeService.bothify("????####@mailinator.com"));
		
		tdImport.setValue(1, 5, fakeService.numerify("9#######"));
		tdImport.setValue(2, 5, fakeService.numerify("9#######"));
		tdImport.setValue(3, 5, fakeService.numerify("9#######"));
	}
	
	public String[] getAddData() {
		testData = new String[6];	
		testData[0] = tdImport.getValue(1, 1);
		testData[1] = tdImport.getValue(1, 2);
		testData[2] = tdImport.getValue(1, 3);
		testData[3] = tdImport.getValue(1, 4);
		testData[4] = tdImport.getValue(1, 5);
		testData[5] = tdImport.getValue(1, 6);
		return testData;
	}
	
	public String[] getEditData() {
		testData = new String[12];		
		testData[0] = tdImport.getValue(2, 1);
		testData[1] = tdImport.getValue(2, 2);
		testData[2] = tdImport.getValue(2, 3);
		testData[3] = tdImport.getValue(2, 4);
		testData[4] = tdImport.getValue(2, 5);
		testData[5] = tdImport.getValue(2, 6);
		testData[6] = tdImport.getValue(3, 1);
		testData[7] = tdImport.getValue(3, 2);
		testData[8] = tdImport.getValue(3, 3);
		testData[9] = tdImport.getValue(3, 4);
		testData[10] = tdImport.getValue(3, 5);
		testData[11] = tdImport.getValue(3, 6);
		return testData;
	}
	
	public String[] getMandatoryMsgDismissData() {
		testData = new String[5];
		testData[0] = tdImport.getValue(4, 1);
		testData[1] = tdImport.getValue(4, 3);
		testData[2] = tdImport.getValue(4, 4);
		testData[3] = tdImport.getValue(4, 5);
		testData[4] = tdImport.getValue(4, 6);
		return testData;
	}
	
	public String[] getLengthValidationData() {
		testData = new String[10];
		testData[0] = tdImport.getValue(5, 1);
		testData[1] = tdImport.getValue(6, 1);
		testData[2] = tdImport.getValue(7, 1);
		testData[3] = tdImport.getValue(8, 1);
		testData[4] = tdImport.getValue(9, 1);
		testData[5] = tdImport.getValue(10, 1);
		
		testData[6] = tdImport.getValue(5, 3);
		testData[7] = tdImport.getValue(5, 4);
		testData[8] = tdImport.getValue(5, 5);
		testData[9] = tdImport.getValue(5, 6);		
		return testData;
	}
		
	public String[] getEmailValidationData() {
		testData = new String[10];
		testData[0] = tdImport.getValue(11, 3);  
		testData[1] = tdImport.getValue(12, 3);
		testData[2] = tdImport.getValue(13, 3);
		testData[3] = tdImport.getValue(14, 3);
		testData[4] = tdImport.getValue(15, 3);
		testData[5] = tdImport.getValue(16, 3);
		
		testData[6] = tdImport.getValue(11, 1);
		testData[7] = tdImport.getValue(11, 4);
		testData[8] = tdImport.getValue(11, 5);
		testData[9] = tdImport.getValue(11, 6);
		return testData;
	}
	
}
