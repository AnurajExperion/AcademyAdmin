package com.academyadmin.testdata;

import java.util.Locale;

import com.academyadmin.baseclass.SetUp;
import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class AddQuestionData extends SetUp {
	Locale local = new Locale("en-IND");
	Faker fake = new Faker(local);
	FakeValuesService fakeService = new FakeValuesService(local, new RandomService());
	TestDataImport tdImport = new TestDataImport();
	String[] testData;
	
	public void generateFakeData() {
		tdImport.setValue(1, 7, fakeService.bothify("???###"));
		tdImport.setValue(2, 7, fakeService.bothify("???###"));
		tdImport.setValue(3, 7, fakeService.bothify("???###"));
		tdImport.setValue(4, 7, fakeService.bothify("???###"));
		tdImport.setValue(5, 7, fakeService.bothify("???###"));
		tdImport.setValue(6, 7, fakeService.bothify("???###"));
	}
	
	public String[] getAddQstnData() {
		testData = new String[17];
		testData[0] = tdImport.getValue(1, 1);
		testData[1] = tdImport.getValue(1, 2);
		testData[2] = tdImport.getValue(1, 3);
		testData[3] = tdImport.getValue(1, 4);
		testData[4] = tdImport.getValue(1, 5);
		testData[5] = tdImport.getValue(1, 6);
		testData[6] = tdImport.getValue(1, 7);
		testData[7] = tdImport.getValue(1, 8);
		testData[8] = tdImport.getValue(1, 9);
		testData[9] = tdImport.getValue(1, 10);
		testData[10] = tdImport.getValue(1, 11);
		testData[11] = tdImport.getValue(1, 12);
		testData[12] = tdImport.getValue(1, 13);
		testData[13] = tdImport.getValue(1, 14);
		testData[14] = tdImport.getValue(1, 15);
		testData[15] = tdImport.getValue(1, 16);
		testData[16] = tdImport.getValue(1, 17);		
		return testData;
	}
	
	public String[] getEditQstnData() {
		testData = new String[9];
		testData[0] = "Hard";
		testData[1] = "Ready to publish";
		testData[2] = "1";
		testData[3] = fakeService.bothify("????####");
		testData[4] = "What is the color of Lilly";
		testData[5] = "White";
		testData[6] = "Lavender";
		testData[7] = "Option 1";
		testData[8] = fake.lorem().characters(20);			
		return testData;
	}
	
	public String[] getDeleteQstnData() {
		testData = new String[17];
		testData[0] = tdImport.getValue(6, 1);
		testData[1] = tdImport.getValue(6, 2);
		testData[2] = tdImport.getValue(6, 3);
		testData[3] = tdImport.getValue(6, 4);
		testData[4] = tdImport.getValue(6, 5);
		testData[5] = tdImport.getValue(6, 6);
		testData[6] = tdImport.getValue(6, 7);
		testData[7] = tdImport.getValue(6, 8);
		testData[8] = tdImport.getValue(6, 9);
		testData[9] = tdImport.getValue(6, 10);
		testData[10] = tdImport.getValue(6, 11);
		testData[11] = tdImport.getValue(6, 12);
		testData[12] = tdImport.getValue(6, 13);
		testData[13] = tdImport.getValue(6, 14);
		testData[14] = tdImport.getValue(6, 15);
		testData[15] = tdImport.getValue(6, 16);
		testData[16] = tdImport.getValue(6, 17);		
		return testData;
	}
}

