package com.academyadmin.testdata;

import com.academyadmin.baseclass.SetUp;

public class DeviceDetails extends SetUp {
	TestDataImport tdImport = new TestDataImport();
	String[] deviceData;
	
	public String[] getDeviceData() {
		deviceData = new String[4];
		deviceData[0] = tdImport.getValue(1, 0);
		deviceData[1] = tdImport.getValue(1, 1);
		deviceData[2] = tdImport.getValue(1, 2);
		deviceData[3] = tdImport.getValue(1, 4);
		return deviceData;
	}
}
