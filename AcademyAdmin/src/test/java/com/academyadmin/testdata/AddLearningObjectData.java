package com.academyadmin.testdata;

import java.util.Locale;

import com.academyadmin.baseclass.SetUp;
import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class AddLearningObjectData extends SetUp {
	Locale local = new Locale("en-IND");
	Faker fake = new Faker(local);
	FakeValuesService fakeService = new FakeValuesService(local, new RandomService());
	TestDataImport tdImport = new TestDataImport();
	String[] testData;
	
	
	public void generateFakeData() {
		
	}
	
	public String[] getAddLearningObectData() {
		testData = new String[10];
		testData[0] = "English";
		testData[1] = fakeService.bothify("LO???###");
		testData[2] = "Name";
		testData[3] = "English";
		testData[4] = "Linguistics";
		testData[5] = "Active";
		testData[6] = fakeService.letterify("???");
		testData[7] = "false";
		testData[8] = fake.lorem().characters(100);
		testData[9] = "What is your name?";
		return testData;
	}
	
}
