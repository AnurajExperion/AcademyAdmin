package com.academyadmin.testdata;

import java.util.Locale;

import com.academyadmin.baseclass.SetUp;
import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class SignInData extends SetUp {

	Locale local = new Locale("en-IND");
	Faker fake = new Faker(local);
	FakeValuesService fakeService = new FakeValuesService(local, new RandomService());
	TestDataImport tdImport = new TestDataImport();
	String[] testData;
	
	public String[] getSignInInvalidData() {
		testData = new String[2];
		testData[0] = tdImport.getValue(1, 1);
		testData[1] = tdImport.getValue(1, 2);
		return testData;
	}
	
	public String[] getSignInValidData() {
		testData = new String[2];
		testData[0] = tdImport.getValue(2, 1);
		testData[1] = tdImport.getValue(2, 2);
		return testData;
	}
}
