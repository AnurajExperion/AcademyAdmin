package com.academyadmin.testdata;

import java.util.Locale;

import com.academyadmin.baseclass.SetUp;
import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class AddCourseData extends SetUp{

	Locale local = new Locale("en-IND");
	Faker fake = new Faker(local);
	FakeValuesService fakeService = new FakeValuesService(local, new RandomService());
	TestDataImport tdImport = new TestDataImport();
	String[] testData;

	public void generateFakeData() {
		
	}

	public String[] getAddCourseData() {		
		testData = new String[19];		
		testData[0] = "English";
		testData[1] = fake.name().title();
		testData[2] = "Grade One - Primary";
		testData[3] = "Semester 1";
		testData[4] = "Syllabus 1";
		testData[5] = "English";
		testData[6] = "Kuwait";
		testData[7] = "Meta information";
		testData[8] = "Description";
		testData[9] = "Learning path";
		testData[10] = "Inactive";
		testData[11] = projectFolder+"/Uploads/Image1.jpg";
		testData[12] = fake.name().title();
		testData[13] = "2";
		testData[14] = "60";		
		return testData;
	}	
}

